CREATE DATABASE thi WITH ENCODING 'UTF8';

\c thi;

SET client_encoding = UTF8;
SET TIMEZONE = 'Asia/Ho_Chi_Minh';

\i database.sql;
\i log-database.sql;

\i functions/log/cauhoi.sql;
\i functions/log/cautraloi.sql;
\i functions/log/chuong.sql;
\i functions/log/dangkylophoc.sql
\i functions/log/dethimonhoc.sql
\i functions/log/lophoc.sql;
\i functions/log/monhoc.sql;
\i functions/log/quyen.sql;
\i functions/log/sinhvien.sql;
\i functions/log/thoigiandethichinh.sql;
\i functions/public/cauhoi.sql;
\i functions/public/cauhoidethi.sql;
\i functions/public/cautraloi.sql;
\i functions/public/chuong.sql;
\i functions/public/dangkylophoc.sql;
\i functions/public/dethi.sql;
\i functions/public/dethimonhoc.sql;
\i functions/public/lophoc.sql;
\i functions/public/monhoc.sql;
\i functions/public/nguoidung.sql;
\i functions/public/quyen.sql;
\i functions/public/sinhvien.sql;
\i functions/public/thoigiandethichinh.sql;

\i procedures/cauhoi.sql;
\i procedures/cauhoidethi.sql;
\i procedures/cautraloi.sql;
\i procedures/chuong.sql;
\i procedures/dangkylophoc.sql;
\i procedures/dethi.sql;
\i procedures/dethimonhoc.sql;
\i procedures/lophoc.sql;
\i procedures/monhoc.sql;
\i procedures/nguoidung.sql;
\i procedures/quyen.sql;
\i procedures/sinhvien.sql;
\i procedures/thoigiandethichinh.sql;

\i triggers/cauhoi.sql;
\i triggers/cauhoidethi.sql;
\i triggers/cautraloi.sql;
\i triggers/chuong.sql;
\i triggers/dangkylophoc.sql;
\i triggers/dethi.sql;
\i triggers/dethimonhoc.sql;
\i triggers/lophoc.sql;
\i triggers/monhoc.sql;
\i triggers/quyen.sql;
\i triggers/sinhvien.sql;
\i triggers/thoigiandethichinh.sql;

\i insert-comments.sql;

\i insert.sql;