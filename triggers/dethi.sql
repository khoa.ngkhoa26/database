CREATE TRIGGER before_insert_dethi
BEFORE INSERT ON DeThi
FOR EACH ROW
EXECUTE FUNCTION prevent_insert_dethi_if_not_time_start();

CREATE TRIGGER before_update_dethi
BEFORE UPDATE ON DeThi
FOR EACH ROW
EXECUTE FUNCTION prevent_update_dethi();

CREATE TRIGGER before_delete_dethi
AFTER DELETE ON DeThi
FOR EACH ROW
EXECUTE FUNCTION prevent_delete_dethi();

CREATE TRIGGER after_insert_dethi
AFTER INSERT ON DeThi
FOR EACH ROW
EXECUTE FUNCTION insert_cauhoidethi_on_dethi_insert();