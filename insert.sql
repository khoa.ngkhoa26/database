-- Insert LopHoc
CALL insert_lophoc('D1');
CALL insert_lophoc('E1');
CALL insert_lophoc('E2');

-- Insert MonHoc
CALL insert_monhoc(1, 'Lịch sử');
CALL insert_monhoc(1, 'Địa lý');
CALL insert_monhoc(2, 'Sinh học');
CALL insert_monhoc(2, 'Hóa học');

-- Insert Chuong môn sử
CALL insert_chuong(1, 1, 'Bối cảnh quốc tế từ sau chiến tranh thế giới thứ hai');
CALL insert_chuong(1, 2, 'Liên Xô và các nước Đông Âu (1945-1991). Liên Bang Nga (1991-2000)');
CALL insert_chuong(1, 3, 'Các nước Á, Phi, và Mĩ La Tinh (1945-2000)');
CALL insert_chuong(1, 4, 'Mĩ, Tây Âu, Nhật Bản (1945-2000)');
CALL insert_chuong(1, 5, 'Quan hệ quốc tế (1945-2000)');

-- Insert môn Địa lí
CALL insert_chuong(2, 1, 'Địa lí tự nhiên');
CALL insert_chuong(2, 2, 'Địa lí dân cư');
CALL insert_chuong(2, 3, 'Địa lí kinh tế');
CALL insert_chuong(2, 4, 'Địa lí các vùng kinh tế');

--Insert môn Sinh học
CALL insert_chuong(3, 1, 'Sinh học phân tử');
CALL insert_chuong(3, 2, 'Sinh học tế bào');
CALL insert_chuong(3, 3, 'Sinh học di truyền');
CALL insert_chuong(3, 4, 'Sinh học động vật');
CALL insert_chuong(3, 5, 'Sinh học Sinh thái');

--Insert môn Hóa học
CALL insert_chuong(4, 1, 'Hóa học cơ bản');
CALL insert_chuong(4, 2, 'Cấu trúc nguyên tử và bảng tuần hoàn');
CALL insert_chuong(4, 3, 'Cấu tạo hạt nhân và phản ứng hạt nhân');
CALL insert_chuong(4, 4, ' Hóa học vô cơ');
CALL insert_chuong(4, 5, 'Hóa học hữu cơ');


/* Lịch sử */
-- Chuong 1
CALL insert_cauhoi(1, 'Hội nghị Ianta (1945) có sự tham gia của các nước nào?');
CALL insert_cautraloi(1, 'Anh - Pháp - Mĩ');
CALL insert_cautraloi(1, 'Anh - Mĩ - Liên Xô');
CALL insert_cautraloi(1, 'Anh - Pháp - Đức');
CALL insert_cautraloi(1, 'Mĩ - Liên Xô - Trung Quốc');
CALL update_cauhoi_cautraloi(1, 2);

CALL insert_cauhoi(1, 'Hội nghị Ianta được triệu tập vào thời điểm nào của cuộc Chiến tranh thế giới thứ hai (1939 – 1945)?');
CALL insert_cautraloi(2, 'Chiến tranh thế giới thứ hai bùng nổ');
CALL insert_cautraloi(2, 'Chiến tranh thế giới thứ hai bước vào giai đoạn ác liệt');
CALL insert_cautraloi(2, 'Chiến tranh thế giới thứ hai bước vào giai đoạn kết thúc');
CALL insert_cautraloi(2, 'Chiến tranh thế giới thứ hai đã kết thúc');
CALL update_cauhoi_cautraloi(2, 7);

CALL insert_cauhoi(1, 'Tương lai của Nhật Bản được quyết định như thế nào theo Hội nghị Ianta (2-1945)?');
CALL insert_cautraloi(3, 'Nhật Bản bị quân đội Mĩ chiếm đóng');
CALL insert_cautraloi(3, 'Nhật Bản vẫn giữ nguyên trạng');
CALL insert_cautraloi(3, 'Quân đội Liên Xô chiếm 4 đảo thuộc quần đảo Curin của Nhật Bản');
CALL insert_cautraloi(3, 'Nhật Bản trở thành thuộc địa kiểu mới của Mĩ');
CALL update_cauhoi_cautraloi(3, 9);

CALL insert_cauhoi(1, 'Theo quyết định của hội nghị Ianta (2-1945), quốc gia nào cần phải trở thành một quốc gia thống nhất và dân chủ?');
CALL insert_cautraloi(4, 'Đức');
CALL insert_cautraloi(4, 'Mông Cổ');
CALL insert_cautraloi(4, 'Trung Quốc');
CALL insert_cautraloi(4, 'Triều Tiên');
CALL update_cauhoi_cautraloi(4, 15);

CALL insert_cauhoi(1, 'Theo quy định của Hội nghị Ianta (2-1945), quốc gia nào sẽ thực hiện nhiệm vụ chiếm đóng, giải giáp miền Tây Đức, Tây Béc-lin và các nước Tây Âu?');
CALL insert_cautraloi(5, 'Liên Xô');
CALL insert_cautraloi(5, 'Mĩ');
CALL insert_cautraloi(5, 'Mĩ, Anh');
CALL insert_cautraloi(5, ' Mĩ, Anh, Pháp');
CALL update_cauhoi_cautraloi(5, 20);

CALL insert_cauhoi(1, 'Theo quy định của Hội nghị Ianta, quân đội nước nào sẽ chiếm đóng các vùng Đông Đức, Đông Âu, Đông Bắc Triều Tiên sau chiến tranh thế giới thứ hai?');
CALL insert_cautraloi(6, 'Liên Xô');
CALL insert_cautraloi(6, 'Mĩ');
CALL insert_cautraloi(6, 'Anh');
CALL insert_cautraloi(6, 'Pháp');
CALL update_cauhoi_cautraloi(6, 21);

CALL insert_cauhoi(1, 'Theo nội dung của Hội nghị Pốtxđam, việc giải giáp quân Nhật ở Đông Dương được giao cho ai?');
CALL insert_cautraloi(7, 'Quân đội Anh trên toàn Việt Nam');
CALL insert_cautraloi(7, 'Quân đội Pháp ở phía Nam vĩ tuyến 16');
CALL insert_cautraloi(7, 'Quân đội Anh ở phía Nam vĩ tuyến 16 và quân đội Trung Hoa Dân quốc vào phía Bắc');
CALL insert_cautraloi(7, 'Quân đội Trung Hoa Dân quốc vào phía Bắc vĩ tuyến 16 và quân đội Pháp ở phía Nam');
CALL update_cauhoi_cautraloi(7, 27);

CALL insert_cauhoi(1, 'Các vùng Đông Nam Á, Nam Á, Tây Á thuộc phạm vi ảnh hưởng của quốc gia nào theo quy định của Hội nghị Ianta (2-1945)?');
CALL insert_cautraloi(8, 'Liên Xô, Mĩ, Anh');
CALL insert_cautraloi(8, 'Các nước phương Tây từng chiếm đóng ở đây');
CALL insert_cautraloi(8, 'Hoa Kỳ, Anh, Pháp');
CALL insert_cautraloi(8, 'Anh, Đức, Nhật Bản');
CALL update_cauhoi_cautraloi(8, 30);

CALL insert_cauhoi(1, 'Nội dung nào không phải là mục đích triệu tập Hội nghị Ianta (tháng 2-1945)?');
CALL insert_cautraloi(9, 'Nhanh chóng đánh bại hoàn toàn các nước phát xít');
CALL insert_cautraloi(9, 'Thành lập khối Đồng minh chống phát xít');
CALL insert_cautraloi(9, 'Tổ chức lại thế giới sau chiến tranh');
CALL insert_cautraloi(9, 'Phân chia thành quả giữa các nước thắng trận');
CALL update_cauhoi_cautraloi(9, 34);

CALL insert_cauhoi(1, 'Đâu không phải là nguyên nhân dẫn tới việc các cường quốc đồng minh triệu tập Hội nghị Ianta (2-1945)?');
CALL insert_cautraloi(10, 'Yêu cầu nhanh chóng đánh bại hoàn toàn các nước phát xít');
CALL insert_cautraloi(10, 'Yêu cầu tổ chức lại thế giới sau chiến tranh');
CALL insert_cautraloi(10, 'Yêu cầu thắt chặt khối đồng minh chống phát xít');
CALL insert_cautraloi(10, 'Phân chia thành quả chiến thắng giữa các nước thắng trận');
CALL update_cauhoi_cautraloi(10, 39);

-- Chuong 2
CALL insert_cauhoi(2, 'Kế hoạch 5 năm (1946-1950) của nhân dân Xô Viết thực hiện trong hoàn cảnh nào?');
CALL insert_cautraloi(11, 'Là nước thắng trận, Liên Xô thu được nhiều thành quả từ trong Chiến tranh thế giới thứ hai.');
CALL insert_cautraloi(11, 'Chiến tranh thế giới thứ hai để lại hậu quả nặng nề.');
CALL insert_cautraloi(11, 'Khôi phục kinh tế, hằn gắn vết thương chiến tranh.');
CALL insert_cautraloi(11, 'Liên Xô cần xây dựng cơ sở vật chất kĩ thuật cho chủ nghĩa xã hội.');
CALL update_cauhoi_cautraloi(11, 42);

CALL insert_cauhoi(2, 'Kế hoạch 5 năm (1946-1950) nhân dân Xô Viết thực hiện nhằm mục đích');
CALL insert_cautraloi(12, 'khôi phục kinh tế, hàn gắt vết thương chiến tranh');
CALL insert_cautraloi(12, 'củng cố quốc phòng an ninh');
CALL insert_cautraloi(12, 'xây dựng cơ sở vật chất kĩ thuật cho chủ nghĩa xã hội');
CALL insert_cautraloi(12, 'công nghiệp hóa xã hội chủ nghĩa');
CALL update_cauhoi_cautraloi(12, 45);

CALL insert_cauhoi(2, 'Kế hoạch 5 năm (1946-1950) của Liên Xô được tiến hành trong thời gian bao lâu?');
CALL insert_cautraloi(13, '4 năm 3 tháng');
CALL insert_cautraloi(13, '1 năm 3 tháng');
CALL insert_cautraloi(13, '12 tháng');
CALL insert_cautraloi(13, '9 tháng');
CALL update_cauhoi_cautraloi(13, 49);

CALL insert_cauhoi(2, 'Kế hoạch 5 năm (1946-1950) được Liên Xô tiến hành đã hoàn thành trước thời hạn bao lâu?');
CALL insert_cautraloi(14, '1 năm 3 tháng');
CALL insert_cautraloi(14, '9 tháng');
CALL insert_cautraloi(14, '12 tháng');
CALL insert_cautraloi(14, '10 tháng');
CALL update_cauhoi_cautraloi(14, 54);

CALL insert_cauhoi(2, 'Sự kiện Liên Xô chế tạo thành công bom nguyên tử (1949) đã');
CALL insert_cautraloi(15, 'Phá vỡ thế độc quyền nguyên tử của Mỹ');
CALL insert_cautraloi(15, 'Làm đảo lộn chiến lược toàn cầu của Mỹ');
CALL insert_cautraloi(15, 'Buộc các nước phương Tây phải nể sợ');
CALL insert_cautraloi(15, 'Khởi đầu sự đối đầu giữa Liên Xô và Mỹ');
CALL update_cauhoi_cautraloi(15, 57);

CALL insert_cauhoi(2, 'Năm 1949, Khoa học - kĩ thuật Liên Xô có bước phát triển nhanh chóng được đánh dấu bằng sự kiện nào?');
CALL insert_cautraloi(16, 'Liên Xô phóng thành cng vệ tinh nhân tạo');
CALL insert_cautraloi(16, 'Liên Xô đưa người bay vào vũ trụ');
CALL insert_cautraloi(16, 'Liên Xô chế tạo thành công bom nguyên tử');
CALL insert_cautraloi(16, 'Liên Xô phóng thành công tàu phương Đông');
CALL update_cauhoi_cautraloi(16, 63);

CALL insert_cauhoi(2, 'Liên Xô trở thành cường quốc công nghiệp thứ hai thế giới trong khoảng thời gian nào?');
CALL insert_cautraloi(17, 'Từ nửa đầu những năm 70 của thế kỉ XX');
CALL insert_cautraloi(17, 'Đến nửa đầu những năm 70 của thế kỉ XX');
CALL insert_cautraloi(17, 'Từ cuối những năm 70 của thế kỉ XX');
CALL insert_cautraloi(17, 'Đến cuối những năm 70 của thế kỉ XX');
CALL update_cauhoi_cautraloi(17, 66);

CALL insert_cauhoi(2, 'Đến nửa đầu những năm 70, Liên Xô đứng ở vị trí nào trong nền kinh tế thế giới?');
CALL insert_cautraloi(18, 'Siêu cường kinh tế duy nhất thế giới');
CALL insert_cautraloi(18, 'Đến nửa đầu những năm 70 của thế kỉ XX');
CALL insert_cautraloi(18, 'Từ cuối những năm 70 của thế kỉ XX');
CALL insert_cautraloi(18, 'Đến cuối những năm 70 của thế kỉ XX');
CALL update_cauhoi_cautraloi(18, 70);

CALL insert_cauhoi(2, 'Sự kiện nào đã mở đầu kỷ nguyên chinh phục vũ trụ của loài người?');
CALL insert_cautraloi(19, 'Liên Xô chế tạo thành công bom nguyên tử');
CALL insert_cautraloi(19, 'Liên Xô phóng tàu vũ trụ đưa nhà du hành vũ trụ Gagarin bay vòng quanh Trái Đất');
CALL insert_cautraloi(19, 'Neil Armstrong đặt chân lên Mặt Trăng');
CALL insert_cautraloi(19, 'Liên Xô phóng thành công trạm tự động “Mặt Trăng 3” (Luna 3) bay vòng quanh phía sau Mặt Trăng');
CALL update_cauhoi_cautraloi(19, 74);

CALL insert_cauhoi(2, 'Từ năm 1950 đến nửa đầu những năm 70, Liên Xô đi đầu thế giới trong lĩnh vực nào?');
CALL insert_cautraloi(20, 'Công nghiệp nặng, công nghiệp chế tạo');
CALL insert_cautraloi(20, 'Công nghiệp sản xuất hàng tiêu dùng');
CALL insert_cautraloi(20, 'Công nghiệp quốc phòng');
CALL insert_cautraloi(20, 'Công nghiệp vũ trụ, công nghiệp điện hạt nhân');
CALL update_cauhoi_cautraloi(20, 80);

-- Chuong 3
CALL insert_cauhoi(3, 'Khu vực Đông Bắc Á bao gồm các quốc gia nào?');
CALL insert_cautraloi(21, 'Trung Quốc, Nhật Bản, Hàn Quốc, Triều Tiên');
CALL insert_cautraloi(21, 'Trung Quốc, Đài Loan, Hàn Quốc, Triều Tiên');
CALL insert_cautraloi(21, 'Trung Quốc, Nhật Bản, Hàn Quốc, Triều Tiên, Đài Loan');
CALL insert_cautraloi(21, 'Trung Quốc, Nhật Bản, Hàn Quốc, Triều Tiên, Đài Loan, vùng Viễn Đông Liên Bang Nga');
CALL update_cauhoi_cautraloi(21, 81);

CALL insert_cauhoi(3, 'Trước Chiến tranh thế giới thứ hai, tình hình các nước Đông Bắc Á như thế nào?');
CALL insert_cautraloi(22, 'Đều bị các nước thực dân xâm lược.');
CALL insert_cautraloi(22, 'Đều là những quốc gia độc lập.');
CALL insert_cautraloi(22, 'Hầu hết đều bị chủ nghĩa thực dân nô dịch.');
CALL insert_cautraloi(22, 'Có nền kinh tế phát triển.');
CALL update_cauhoi_cautraloi(22, 87);

CALL insert_cauhoi(3, 'Những sự kiện thể hiện sự biến đổi lớn về chính trị của khu vực Đông Bắc Á sau chiến tranh thế giới thứ hai là');
CALL insert_cautraloi(23, 'Trung Quốc thu hồi được Hồng Công');
CALL insert_cautraloi(23, 'Nhật Bản chủ trương liên minh chặt chẽ với Mĩ');
CALL insert_cautraloi(23, 'Sự ra đời của nước CHND Trung Hoa và sự thành lập hai nhà nước trên bán đảo Triều Tiên');
CALL insert_cautraloi(23, 'Mĩ phát động chiến tranh xâm lược Triều Tiên');
CALL update_cauhoi_cautraloi(23, 91);

CALL insert_cauhoi(3, 'Các quốc gia và vùng lãnh thổ nào ở khu vực Đông Bắc Á được mệnh danh là “con rồng” kinh tế châu');
CALL insert_cautraloi(24, 'Hàn Quốc, Nhật Bản, Hồng Công');
CALL insert_cautraloi(24, 'Nhật Bản, Hồng Công, Đài Loan');
CALL insert_cautraloi(24, 'Hàn Quốc, Đài Loan, Hồng Công');
CALL insert_cautraloi(24, 'Hàn Quốc, Nhật Bản, Đài Loan');
CALL update_cauhoi_cautraloi(24, 95);

CALL insert_cauhoi(3, 'Quốc gia và vùng lãnh thổ nào dưới đây không được mệnh danh là “con rồng” kinh tế của châu Á?');
CALL insert_cautraloi(25, 'Hàn Quốc');
CALL insert_cautraloi(25, 'Đài Loan');
CALL insert_cautraloi(25, 'Hồng Công');
CALL insert_cautraloi(25, 'Nhật Bản');
CALL update_cauhoi_cautraloi(25, 100);

CALL insert_cauhoi(3, 'Bán đảo Triều Tiên bị chia cắt thành 2 miền theo vĩ tuyến số bao nhiêu?');
CALL insert_cautraloi(26, 'Vĩ tuyến 39');
CALL insert_cautraloi(26, 'Vĩ tuyến 38');
CALL insert_cautraloi(26, 'Vĩ tuyến 16');
CALL insert_cautraloi(26, 'Vĩ tuyến 37');
CALL update_cauhoi_cautraloi(26, 102);

CALL insert_cauhoi(3, 'Nhà nước Đại Hàn Dân quốc (Hàn Quốc) được thành lập vào thời gian nào và ở đâu?');
CALL insert_cautraloi(27, 'Tháng 8 - 1948, ở phía Nam bán đảo Triều Tiên');
CALL insert_cautraloi(27, 'Tháng 9 - 1948, ở phía Bắc bán đảo Triều Tiên');
CALL insert_cautraloi(27, 'Tháng 8 - 1949, ở phía Nam bán đảo Triều Tiên');
CALL insert_cautraloi(27, 'Tháng 9 - 1949, ở phía Bắc bán đảo Triều Tiên');
CALL update_cauhoi_cautraloi(27, 105);

CALL insert_cauhoi(3, 'Trong những năm 1950-1953, hai miền bán đảo Triều Tiên ở trong tình thế');
CALL insert_cautraloi(28, 'Hòa dịu, hợp tác');
CALL insert_cautraloi(28, 'Tháng 9 - 1948, ở phía Bắc bán đảo Triều Tiên');
CALL insert_cautraloi(28, 'Tháng 8 - 1949, ở phía Nam bán đảo Triều Tiên');
CALL insert_cautraloi(28, 'Tháng 9 - 1949, ở phía Bắc bán đảo Triều Tiên');
CALL update_cauhoi_cautraloi(28, 112);

CALL insert_cauhoi(3, 'Hiệp định hòa hợp giữa hai miền Nam - Bắc Triều Tiên được kí kết từ năm 2000 có ý nghĩa gì?');
CALL insert_cautraloi(29, 'Mở ra thời kì hợp tác cùng phát triển giữa hai miền Nam - Bắc Triều Tiên');
CALL insert_cautraloi(29, 'Mở ra bước mới trong tiến trình hòa hợp, thống nhất bán đảo Triều Tiên');
CALL insert_cautraloi(29, 'Chấm dứt thời kì đối đầu căng thẳng giữa hai miền');
CALL insert_cautraloi(29, 'Chấm dứt tình trạng chia cắt, thống nhất bán đảo Triều Tiên');
CALL update_cauhoi_cautraloi(29, 114);

CALL insert_cauhoi(3, 'Đâu không phải lý do tại sao cho đến nay Đài Loan vẫn nằm ngoài sự kiểm soát của Cộng hòa Nhân dân Trung Hoa?');
CALL insert_cautraloi(30, 'Do Quốc dân Đảng vẫn nắm quyền kiểm soát khu vực này sau cuộc nội chiến 1946 - 1949');
CALL insert_cautraloi(30, 'Mở ra bước mới trong tiến trình hòa hợp, thống nhất bán đảo Triều Tiên');
CALL insert_cautraloi(30, 'Do sự chia rẽ của các thế lực thù địch');
CALL insert_cautraloi(30, 'Do đường lối “một đất nước hai chế độ” nhà nước CHND Trung Hoa muốn thực hiện');
CALL update_cauhoi_cautraloi(30, 119);

-- Chuong 4
CALL insert_cauhoi(4, 'Hậu quả của Chiến tranh thế giới thứ hai (1939-1945) để lại đã làm cho nền kinh tế Tây Âu trở nên');
CALL insert_cautraloi(31, 'Kiệt quệ');
CALL insert_cautraloi(31, 'Phát triển mạnh mẽ');
CALL insert_cautraloi(31, 'Phát triển không ổn định');
CALL insert_cautraloi(31, 'Phát triển chậm');
CALL update_cauhoi_cautraloi(31, 121);

CALL insert_cauhoi(4, 'Nguyên nhân cơ bản giúp kinh tế Tây Âu phát triển sau chiến tranh thế giới thứ 2 là');
CALL insert_cautraloi(32, 'Nguồn viện trợ của Mỹ thông qua kế hoạch Macsan');
CALL insert_cautraloi(32, 'Tài nguyên thiên nhiên giàu có, nhân lực lao động dồi dào');
CALL insert_cautraloi(32, 'Tận dụng tốt cơ hội bên ngoài và áp dụng thành công khoa học kỹ thuật');
CALL insert_cautraloi(32, 'Quá trình tập trung tư bản và tập trung lao động cao');
CALL update_cauhoi_cautraloi(32, 127);

CALL insert_cauhoi(4, 'Năm 1947, Mĩ đề ra và thực hiện “kế hoạch Mácsan” nhằm mục đích chính trị gì?');
CALL insert_cautraloi(33, 'Tạo ra căn cứ tiền phương chống Liên Xô');
CALL insert_cautraloi(33, 'Tạo ra sự đối trọng với khối Đông Âu xã hội chủ nghĩa');
CALL insert_cautraloi(33, 'Tìm kiếm đồng minh chống lại Liên Xô và Đông Âu');
CALL insert_cautraloi(33, 'Củng cố ảnh hưởng của Mĩ ở châu Âu');
CALL update_cauhoi_cautraloi(33, 131);

CALL insert_cauhoi(4, 'Từ năm 1945 đến 1950, các nước tư bản Tây Âu dựa vào đâu để đạt được sự phục hồi cơ bản về mọi mặt?');
CALL insert_cautraloi(34, 'Hợp tác thành công với Nhật');
CALL insert_cautraloi(34, 'Mở rộng quan hệ với Liên Xô');
CALL insert_cautraloi(34, 'Viện trợ của Mĩ qua kế hoạch Macsan');
CALL insert_cautraloi(34, 'Đẩy mạnh xuất khẩu hàng hóa đến các nước thứ 3');
CALL update_cauhoi_cautraloi(34, 134);

CALL insert_cauhoi(4, 'Đến đầu thập kỉ 70 của thế kỉ XX, Tây Âu đã đạt được thành tựu gì quan trọng về kinh tế?');
CALL insert_cautraloi(35, 'Trở thành khối kinh tế đứng thứ hai thế giới');
CALL insert_cautraloi(35, 'Trở thành một trong ba trung tâm kinh tế- tài chính của thế giới');
CALL insert_cautraloi(35, 'Trở thành trung tâm kinh tế đứng đầu khối tư bản chủ nghĩa');
CALL insert_cautraloi(35, 'Trở thành trung tâm công nghiệp - quốc phòng lớn nhất thế giới');
CALL update_cauhoi_cautraloi(35, 138);

CALL insert_cauhoi(4, 'Từ năm 1973 - 1991, kinh tế của các nước tư bản Tây Âu');
CALL insert_cautraloi(36, 'Lâm vào khủng hoảng, suy thoái, phát triển không ổn định');
CALL insert_cautraloi(36, 'Phát triển ổn định và đạt mức tăng trưởng cao');
CALL insert_cautraloi(36, 'Phát triển không đồng đều do sự sụp đổ của hệ thống thuộc địa');
CALL insert_cautraloi(36, 'Vươn lên hàng thứ hai thế giới');
CALL update_cauhoi_cautraloi(36, 141);

CALL insert_cauhoi(4, 'Điểm nhất quán trong chính sách đối ngoại của các nước Tây Âu giai đoạn 1945-1950 là');
CALL insert_cautraloi(37, 'Mở rộng hợp tác với Nhật Bản và Hàn Quốc');
CALL insert_cautraloi(37, 'Liên kết chống lại các nước Đông Âu');
CALL insert_cautraloi(37, 'Liên minh với CHLB Đức');
CALL insert_cautraloi(37, 'Liên minh chặt chẽ với Mĩ');
CALL update_cauhoi_cautraloi(37, 148);

CALL insert_cauhoi(4, 'Trong giai đoạn 1991 - 2000 ở Tây Âu, những nước nào đã trở thành đối trọng với Mỹ trong nhiều vấn đề quốc tế quan trọng?');
CALL insert_cautraloi(38, 'Anh, Pháp');
CALL insert_cautraloi(38, 'Pháp, Đức');
CALL insert_cautraloi(38, 'Anh, Hà Lan');
CALL insert_cautraloi(38, 'Đức, Anh');
CALL update_cauhoi_cautraloi(38, 150);

CALL insert_cauhoi(4, 'Sau Chiến tranh thế giới thứ hai (1939-1945), các nước Tây Âu có hành động gì đối với các thuộc địa thuộc địa cũ?');
CALL insert_cautraloi(39, 'Đa số ủng hộ vấn đề độc lập ở các thuộc địa');
CALL insert_cautraloi(39, 'Tìm cách biến các nước thuộc thế giới thứ ba thành thuộc địa kiểu mới');
CALL insert_cautraloi(39, 'Ủng hộ việc thiết lập quyền tự trị ở các thuộc địa');
CALL insert_cautraloi(39, 'Tìm cách tái thiết lập chủ quyền ở các thuộc địa cũ');
CALL update_cauhoi_cautraloi(39, 154);

CALL insert_cauhoi(4, 'Chính sách đối ngoại chủ yếu của Tây Âu từ 1950 đến 1973 là gì?');
CALL insert_cautraloi(40, 'Cố gắng quan hệ với Nhật Bản');
CALL insert_cautraloi(40, 'Đa phương hóa trong quan hệ');
CALL insert_cautraloi(40, 'Liên minh hoàn toàn với Mỹ');
CALL insert_cautraloi(40, 'Rút ra khỏi NATO');
CALL update_cauhoi_cautraloi(40, 158);

-- Chuong 5
CALL insert_cauhoi(5, 'Sau chiến tranh thế giới thứ hai, quan hệ giữa Mĩ và Liên Xô đã có sự chuyển biến như thế nào?');
CALL insert_cautraloi(41, 'Chuyển từ đối đầu sang đối thoại, thực hiện hợp tác trên nhiều lĩnh vực');
CALL insert_cautraloi(41, 'Hợp tác với nhau trong việc giải quyết nhiều vấn đề quốc tế lớn');
CALL insert_cautraloi(41, 'Từ hợp tác sang đối đầu trực tiếp với các cuộc chiến tranh cục bộ lớn diễn ra');
CALL insert_cautraloi(41, 'Từ đồng minh trong chiến tranh chuyển sang đối đầu và đi đến tình trạng chiến tranh lạnh');
CALL update_cauhoi_cautraloi(41, 164);

CALL insert_cauhoi(5, 'Nguyên nhân dẫn đến sự đối đầu giữa hai cường quốc Liên Xô và Mĩ là');
CALL insert_cautraloi(42, 'Sự đối lập về mục tiêu và chiến lược');
CALL insert_cautraloi(42, 'Sự đối lập về chế độ chính trị');
CALL insert_cautraloi(42, 'Sự đối lập về khuynh hướng phát triển');
CALL insert_cautraloi(42, 'Sự đối lập về chính sách đối nội, đối ngoại');
CALL update_cauhoi_cautraloi(42, 165);

CALL insert_cauhoi(5, 'Sự kiện nào được xem là khởi đầu cho chính sách chống Liên Xô, gây nên cuộc Chiến tranh lạnh?');
CALL insert_cautraloi(43, 'Thông điệp của Tổng thống Truman tại Quốc hội Mĩ (1947)');
CALL insert_cautraloi(43, 'Kế hoạch Mácsan (1947)');
CALL insert_cautraloi(43, 'Sự ra đời của tổ chức Hiệp ước Bắc Đại Tây Dương (NATO) (1949)');
CALL insert_cautraloi(43, 'Sự ra đời của tổ chức Hiệp ước VACSAVA (1955)');
CALL update_cauhoi_cautraloi(43, 169);

CALL insert_cauhoi(5, 'Sự kiện nào xác lập Chiến tranh lạnh bao trùm cả thế giới sau chiến tranh thế giới thứ hai?');
CALL insert_cautraloi(44, 'Học thuyết của tổng thống Truman');
CALL insert_cautraloi(44, 'Học thuyết của Tổng thống Ri-gân');
CALL insert_cautraloi(44, 'Sự ra đời của NATO và Vacsava');
CALL insert_cautraloi(44, 'Chiến lược cam kết và mở rộng');
CALL update_cauhoi_cautraloi(44, 175);

CALL insert_cauhoi(5, 'Đến đầu thập kỉ 70 của thế kỉ XX, Tây Âu đã đạt được thành tựu gì quan trọng về kinh tế?');
CALL insert_cautraloi(45, 'Trở thành khối kinh tế đứng thứ hai thế giới');
CALL insert_cautraloi(45, 'Trở thành một trong ba trung tâm kinh tế- tài chính của thế giới');
CALL insert_cautraloi(45, 'Trở thành trung tâm kinh tế đứng đầu khối tư bản chủ nghĩa');
CALL insert_cautraloi(45, 'Trở thành trung tâm công nghiệp - quốc phòng lớn nhất thế giới');
CALL update_cauhoi_cautraloi(45, 178);

CALL insert_cauhoi(5, 'Sự ra đời của tổ chức hiệp ước Bắc Đại Tây Dương (1949) và tổ chức hiệp ước Vacsava (1955) đã tác động như thế nào đến quan hệ quốc tế?');
CALL insert_cautraloi(46, 'Đặt nhân loại đứng trước nguy cơ của cuộc chiến tranh thế giới mới');
CALL insert_cautraloi(46, 'Xác lập cục diện hai phe, hai cực, chiến tranh lạnh bao trùm thế giới');
CALL insert_cautraloi(46, 'Đánh dấu cuộc chiến tranh lạnh chính thức bắt đầu');
CALL insert_cautraloi(46, 'Tạo nên sự phân chia đối lập giữa Đông Âu và Tây Âu');
CALL update_cauhoi_cautraloi(46, 182);

CALL insert_cauhoi(5, 'Từ đầu những năm 70 của thế kỉ XX, tình hình quan hệ quốc tế đã có chuyển biến gì?');
CALL insert_cautraloi(47, 'Chuyển từ đối đầu sang đối thoại');
CALL insert_cautraloi(47, 'Tiếp tục đối đầu căng thẳng');
CALL insert_cautraloi(47, 'Xu hướng hòa hoãn xuất hiện');
CALL insert_cautraloi(47, 'Thiết lập quan hệ đồng minh');
CALL update_cauhoi_cautraloi(47, 188);

CALL insert_cauhoi(5, 'Định ước Henxiki (năm 1975) được ký kết giữa');
CALL insert_cautraloi(48, 'Mỹ - Anh - Pháp - Cộng hòa Dân chủ Đức và Liên Xô');
CALL insert_cautraloi(48, '33 nước châu Âu cùng với Mỹ và Canada');
CALL insert_cautraloi(48, 'Các nước châu Âu');
CALL insert_cautraloi(48, 'Cộng hòa Dân chủ Đức, Mỹ, Canada');
CALL update_cauhoi_cautraloi(48, 190);

CALL insert_cauhoi(5, 'Tháng 12-1989 đã diễn ra sự kiện lịch sử gì trong quan hệ quốc tế?');
CALL insert_cautraloi(49, 'Trật tự hai cực Ianta sụp đổ');
CALL insert_cautraloi(49, 'Nước Đức được thống nhất');
CALL insert_cautraloi(49, 'Bức tường Béc lin sụp đổ');
CALL insert_cautraloi(49, 'Chiến tranh lạnh chấm dứt');
CALL update_cauhoi_cautraloi(49, 196);

CALL insert_cauhoi(5, 'Tháng 11-1972 đã diễn ra sự kiện lịch sử gì trong quan hệ quốc tế?');
CALL insert_cautraloi(50, 'Kí kết hiệp định về những cơ sở của quan hệ giữa Đông Đức và Tây Đức');
CALL insert_cautraloi(50, 'Kí kết hiệp ước về việc hạn chế hệ thống phòng chống tên lửa (ABM)');
CALL insert_cautraloi(50, 'Kí kết Định ước Henxinki');
CALL insert_cautraloi(50, 'Cuộc gặp gỡ cấp cao giữa hai nhà lãnh đạo Mĩ và Liên Xô');
CALL update_cauhoi_cautraloi(50, 197);

-- Insert cauhoi môn địa lí
CALL insert_cauhoi(6, 'Nước ta nằm ở vị trí:');
CALL insert_cautraloi(51, 'rìa phía Đông của bán đảo Đông Dương');
CALL insert_cautraloi(51, 'rìa phía Tây của bán đảo Đông Dương.');
CALL insert_cautraloi(51, 'trung tâm châu Á');
CALL insert_cautraloi(51, 'phía đông Đông Nam Á');
CALL update_cauhoi_cautraloi(51, 201);

CALL insert_cauhoi(6, 'Nằm ở rìa phía Đông của bán đảo Đông Dương là nước:');
CALL insert_cautraloi(52, 'Lào');
CALL insert_cautraloi(52, 'Campuchia');
CALL insert_cautraloi(52, 'Việt Nam');
CALL insert_cautraloi(52, 'Mi-an-ma');
CALL update_cauhoi_cautraloi(52, 207);

CALL insert_cauhoi(6, 'Điểm cực Bắc của nước ta là xã Lũng Cú thuộc tỉnh:');
CALL insert_cautraloi(53, 'Cao Bằng');
CALL insert_cautraloi(53, 'Hà Giang');
CALL insert_cautraloi(53, 'Yên Bái');
CALL insert_cautraloi(53, 'Lạng Sơn');
CALL update_cauhoi_cautraloi(53, 210);

CALL insert_cauhoi(6, 'Vị trí địa lí của nước ta là:');
CALL insert_cautraloi(54, 'nằm ở phía Đông bán đảo Đông Dương, gần trung tâm khu vực Đông Nam Á');
CALL insert_cautraloi(54, 'nằm ở phía Tây bán đảo Đông Dương, gần trung tâm khu vực Đông Nam Á');
CALL insert_cautraloi(54, 'nằm ở phía Đông bán đảo Đông Dương, gần trung tâm khu vực châu Á');
CALL insert_cautraloi(54, 'nằm ở phía Tây bán đảo Đông Dương, gần trung tâm khu vực châu Á');
CALL update_cauhoi_cautraloi(54, 213);

CALL insert_cauhoi(6, 'Điểm cực Đông của nước ta là xã Vạn Thạnh thuộc tỉnh:');
CALL insert_cautraloi(55, 'Ninh Thuận');
CALL insert_cautraloi(55, 'Khánh Hòa');
CALL insert_cautraloi(55, 'Đà Nẵng');
CALL insert_cautraloi(55, 'Phú Yên');
CALL update_cauhoi_cautraloi(55, 218);

CALL insert_cauhoi(6, 'Ở tỉnh Khánh Hòa có một đặc điểm tự nhiên rất đặc biệt là:');
CALL insert_cautraloi(56, 'Là tỉnh duy nhất có nhiều đảo');
CALL insert_cautraloi(56, 'Là tỉnh có điểm cực Đông nước ta');
CALL insert_cautraloi(56, 'Là tỉnh có nhiều hải sản nhất');
CALL insert_cautraloi(56, 'Là tỉnh có nhiều than nhất');
CALL update_cauhoi_cautraloi(56, 222);

CALL insert_cauhoi(6, 'Đâu không phải là đặc điểm của vị trí địa lí nước ta:');
CALL insert_cautraloi(57, 'vừa gắn liền với lục địa Á – Âu, vừa tiếp giáp với Thái Bình Dương');
CALL insert_cautraloi(57, 'nằm trên các tuyến đường giao thông hàng hải, đường bộ, đường hàng không quốc');
CALL insert_cautraloi(57, 'trong khu vực có nền kinh tế năng động của thế giới');
CALL insert_cautraloi(57, 'nằm ở trung tâm của châu Á');
CALL update_cauhoi_cautraloi(57, 228);

CALL insert_cauhoi(6, 'Nước ta nằm ở vị trí:');
CALL insert_cautraloi(58, 'vừa gắn liền với lục địa Á – Âu, vừa tiếp giáp với Thái Bình Dương');
CALL insert_cautraloi(58, 'nằm trên các tuyến đường giao thông hàng hải, đường bộ, đường hàng không quốc');
CALL insert_cautraloi(58, 'trong khu vực có nền kinh tế năng động của thế giới');
CALL insert_cautraloi(58, 'nằm ở trung tâm của châu Á');
CALL update_cauhoi_cautraloi(58, 229);

CALL insert_cauhoi(6, 'Đặc điểm nào sau đây không đúng với lãnh thổ nước ta');
CALL insert_cautraloi(59, 'Nằm hoàn toàn trong vùng nhiệt đới nửa cầu Bắc');
CALL insert_cautraloi(59, 'Nằm trọn trong múi giờ số 8');
CALL insert_cautraloi(59, 'Nằm trong vùng có khí hậu nhiệt đới ẩm gió mùa');
CALL insert_cautraloi(59, 'Nằm trong vùng chịu ảnh hưởng của gió Mậu dịch');
CALL update_cauhoi_cautraloi(59, 234);

CALL insert_cauhoi(6, 'Nước ta nằm trong múi giờ thứ mấy?');
CALL insert_cautraloi(60, '5');
CALL insert_cautraloi(60, '6');
CALL insert_cautraloi(60, '7');
CALL insert_cautraloi(60, '8');
CALL update_cauhoi_cautraloi(60, 239);

CALL insert_cauhoi(6, 'Đâu không phải là đặc điểm chung của vùng núi Đông Bắc:');
CALL insert_cautraloi(61, 'địa hình đồi núi thấp chiếm phần lớn diện tích lãnh thổ.');
CALL insert_cautraloi(61, 'có 4 cánh cung lớn chụm lại ở Tam Đảo.');
CALL insert_cautraloi(61, 'gồm các dãy núi song song và so le hướng Tây Bắc – Đông Nam.');
CALL insert_cautraloi(61, 'giáp biên giới Việt - Trung là các khối núi đá vôi đồ sộ.');
CALL update_cauhoi_cautraloi(61, 243);

CALL insert_cauhoi(6, 'Độ cao núi của Trường Sơn Bắc so với Trường Sơn Nam:');
CALL insert_cautraloi(62, 'Trường Sơn Bắc có địa hình núi cao hơn Trường Sơn Nam');
CALL insert_cautraloi(62, 'Trường sơn Bắc chủ yếu là núi thấp, trung bình; Trường Sơn Nam gồm khối núi cao đồ sộ.');
CALL insert_cautraloi(62, 'Trường Sơn Bắc địa hình núi dưới 2000m, Trường Sơn Nam có đỉnh núi cao nhất trên 3000m');
CALL insert_cautraloi(62, 'Trường Sơn Nam có núi cao hơn Trường Sơn Bắc và cao nhất cả nước');
CALL update_cauhoi_cautraloi(62, 246);

CALL insert_cauhoi(6, 'Đặc điểm nào sau đây không phải của dải đồng bằng ven biển miền Trung?');
CALL insert_cautraloi(63, 'Hẹp ngang.');
CALL insert_cautraloi(63, 'Bị chia cắt thành nhiều đồng bằng nhỏ.');
CALL insert_cautraloi(63, 'Chỉ có một số đồng bằng được mở rộng ở các cửa sông lớn.');
CALL insert_cautraloi(63, 'Được hình thành chủ yếu do các sông bồi đắp.');
CALL update_cauhoi_cautraloi(63, 252);

CALL insert_cauhoi(6, 'Đặc điểm nào sau đây không phải của dải đồng bằng ven biển miền Trung?');
CALL insert_cautraloi(64, 'Hẹp ngang.');
CALL insert_cautraloi(64, 'Bị chia cắt thành nhiều đồng bằng nhỏ.');
CALL insert_cautraloi(64, 'Chỉ có một số đồng bằng được mở rộng ở các cửa sông lớn.');
CALL insert_cautraloi(64, 'Được hình thành chủ yếu do các sông bồi đắp.');
CALL update_cauhoi_cautraloi(64, 254);

CALL insert_cauhoi(6, 'Căn cứ vào Atlat Địa lí Việt Nam trang 6 -7, hãy cho biết đồng bằng Nghệ An được hình thành do phù sa của sông nào bồi đắp?');
CALL insert_cautraloi(65, 'sông Mã – Chu');
CALL insert_cautraloi(65, 'sông Cả.');
CALL insert_cautraloi(65, 'sông Gianh.');
CALL insert_cautraloi(65, 'sông Thu Bồn.');
CALL update_cauhoi_cautraloi(65, 257);

CALL insert_cauhoi(7, 'Trong khu vực Đông Nam Á, dân số nước ta xếp thứ 3 sau');
CALL insert_cautraloi(66, 'In-đô-nê-xi-a và Phi-lip-pin.');
CALL insert_cautraloi(66, 'In-đô-nê-xi-a và Thái Lan.');
CALL insert_cautraloi(66, 'In-đô-nê-xi-a và Mi-an-ma.');
CALL insert_cautraloi(66, 'In-đô-nê-xi-a và Ma-lai-xi-a.');
CALL update_cauhoi_cautraloi(66, 261);

CALL insert_cauhoi(7, 'Dân số nước ta đứng thứ mấy trong khu vực Đông Nam Á?');
CALL insert_cautraloi(67, 'Thứ nhất.');
CALL insert_cautraloi(67, 'Thứ hai.');
CALL insert_cautraloi(67, 'Thứ ba.');
CALL insert_cautraloi(67, 'Thứ tư.');
CALL update_cauhoi_cautraloi(67, 267);

CALL insert_cauhoi(7, 'Căn cứ vào Atlat Địa lí Việt Nam trang 16, hãy cho biết dân tộc ít người nào có số dân lớn nhất?');
CALL insert_cautraloi(68, 'Tày');
CALL insert_cautraloi(68, 'Thái');
CALL insert_cautraloi(68, 'Mường');
CALL insert_cautraloi(68, 'Khơ – me');
CALL update_cauhoi_cautraloi(68, 269);

CALL insert_cauhoi(7, 'Căn cứ vào Atlat trang 16, dân tộc ít người có dân số đứng thứ 3 ở Việt Nam là');
CALL insert_cautraloi(69, 'Tày');
CALL insert_cautraloi(69, 'Thái');
CALL insert_cautraloi(69, 'Mường');
CALL insert_cautraloi(69, 'Hmong.');
CALL update_cauhoi_cautraloi(69, 275);

CALL insert_cauhoi(7, 'Vùng có mật độ dân số cao nhất cả nước ta là');
CALL insert_cautraloi(70, 'Đồng bằng sông Hồng.');
CALL insert_cautraloi(70, 'Đồng bằng sông Cửu Long.');
CALL insert_cautraloi(70, 'Duyên hải miền Trung.');
CALL insert_cautraloi(70, 'Đông Nam Bộ.');
CALL update_cauhoi_cautraloi(70, 277);

CALL insert_cauhoi(7, 'Đặc điểm nổi bật về dân cư của Đồng bằng sông Hồng là');
CALL insert_cautraloi(71, 'mật độ dân số cao nhất nước ta.');
CALL insert_cautraloi(71, 'mật độ dân số thấp nhất nước ta.');
CALL insert_cautraloi(71, 'dân cư phân bố đồng đều giữa thành thị và nông thôn.');
CALL insert_cautraloi(71, 'tỉ lệ dân số thành thị cao hơn dân nông thôn.');
CALL update_cauhoi_cautraloi(71, 281);

CALL insert_cauhoi(7, 'Vùng có mật đô dân số thấp nhất hiện nay của nước ta là:');
CALL insert_cautraloi(72, 'Tây Bắc.');
CALL insert_cautraloi(72, 'Đông Nam Bộ.');
CALL insert_cautraloi(72, 'Bắc Trung Bộ.');
CALL insert_cautraloi(72, 'Tây Nguyên.');
CALL update_cauhoi_cautraloi(72, 285);

CALL insert_cauhoi(7, 'Người Việt Nam ở nước ngoài tập trung nhiều nhất tại các quốc gia và khu vực nào sau đây?');
CALL insert_cautraloi(73, 'Trung Á, châu Âu, Ôxtrâylia.');
CALL insert_cautraloi(73, 'Bắc Mĩ, châu Âu, Nam Á.');
CALL insert_cautraloi(73, 'Bắc Mĩ, Ôxtrâylia, Đông Á.');
CALL insert_cautraloi(73, 'Bắc Mĩ, châu Âu, Ôxtrâylia.');
CALL update_cauhoi_cautraloi(73, 292);

CALL insert_cauhoi(7, 'Căn cứ vào Atlat Địa lí Việt Nam trang 15, hãy cho biết nhận xét nào sau đây đúng về sự thay đổi hình dạng tháp dân số năm 2007 so với năm 1999?');
CALL insert_cautraloi(74, 'đáy tháp và đỉnh tháp thu hẹp, thân tháp mở rộng.');
CALL insert_cautraloi(74, 'đáy tháp mở rộng , thân tháp thu hẹp.');
CALL insert_cautraloi(74, 'đáy tháp thu hẹp, thân tháp mở rộng, đỉnh tù.');
CALL insert_cautraloi(74, 'đáy tháp thu hẹp, thân tháp mở rộng, đỉnh nhọn hơn.');
CALL update_cauhoi_cautraloi(74, 295);

CALL insert_cauhoi(7, 'Căn cứ vào Atlat Địa lí Việt Nam trang 15, hãy cho biết nhận xét nào sau đây đúng về sự thay đổi hình dạng tháp dân số năm 2007 so với năm 1999?');
CALL insert_cautraloi(75, 'Đáy tháp thu hẹp.');
CALL insert_cautraloi(75, 'Đáy tháp mở rộng.');
CALL insert_cautraloi(75, 'Đỉnh tháp nhọn.');
CALL insert_cautraloi(75, 'Thân tháp thu hẹp.');
CALL update_cauhoi_cautraloi(75, 297);

/*Sinh học*/
--Chương 10--
CALL insert_cauhoi(10, 'Cấu trúc cơ bản của DNA bao gồm những phần tử nào?');
CALL insert_cautraloi(76, 'Nucleotide');
CALL insert_cautraloi(76, 'Amino acid');
CALL insert_cautraloi(76, 'Glucose');
CALL insert_cautraloi(76, 'Lipid');
CALL update_cauhoi_cautraloi(76, 301);

CALL insert_cauhoi(10, 'Quá trình mã hóa thông tin gen diễn ra ở đâu trong tế bào?');
CALL insert_cautraloi(77, 'Mitochondria');
CALL insert_cautraloi(77, 'Nucleus');
CALL insert_cautraloi(77, 'Endoplasmic reticulum');
CALL insert_cautraloi(77, 'Ribosome');
CALL update_cauhoi_cautraloi(77, 306);

CALL insert_cauhoi(10, 'Quá trình tổng hợp protein diễn ra theo trình tự nào sau đây?');
CALL insert_cautraloi(78, 'Transcription - Translation - Replication');
CALL insert_cautraloi(78, 'Replication - Translation - Transcription');
CALL insert_cautraloi(78, 'Translation - Transcription - Replication');
CALL insert_cautraloi(78, 'Replication - Transcription - Translation');
CALL update_cauhoi_cautraloi(78, 312);

CALL insert_cauhoi(10, 'Cơ chế di truyền tạo nên đa dạng gen ở các loài là do quá trình nào sau đây?');
CALL insert_cautraloi(79, 'Mutagenesis');
CALL insert_cautraloi(79, 'Recombination');
CALL insert_cautraloi(79, 'DNA replication');
CALL insert_cautraloi(79, 'Transcription');
CALL update_cauhoi_cautraloi(79, 314);

CALL insert_cauhoi(10, 'Trong chuỗi DNA, nucleotide nào không thể ghép nối với adenine (A)');
CALL insert_cautraloi(80, 'Thymine (T)');
CALL insert_cautraloi(80, 'Cytosine (C)');
CALL insert_cautraloi(80, 'Guanine (G)');
CALL insert_cautraloi(80, ' Uracil (U)');
CALL update_cauhoi_cautraloi(80, 320);

CALL insert_cauhoi(10, 'Cấu trúc tế bào thực vật khác biệt với tế bào động vật ở điểm nào sau đây?');
CALL insert_cautraloi(81, 'Thành tế bào');
CALL insert_cautraloi(81, 'Mitochondria');
CALL insert_cautraloi(81, 'Nucleus');
CALL insert_cautraloi(81, 'Thành tế bào bao phủ');
CALL update_cauhoi_cautraloi(81, 324);

CALL insert_cauhoi(10, 'Loại tế bào nào có khả năng tự tái tạo và chia để tạo ra các tế bào mới?');
CALL insert_cautraloi(82, 'Tế bào não');
CALL insert_cautraloi(82, 'Tế bào tim');
CALL insert_cautraloi(82, 'Tế bào gốc');
CALL insert_cautraloi(82, 'Tế bào cơ');
CALL update_cauhoi_cautraloi(82, 327);

CALL insert_cauhoi(10, 'Chức năng chính của mitochondria trong tế bào là gì?');
CALL insert_cautraloi(83, 'Tạo năng lượng');
CALL insert_cautraloi(83, ' Tổ chức cấu trúc tế bào');
CALL insert_cautraloi(83, ' Tạo RNA');
CALL insert_cautraloi(83, 'Tổng hợp protein');
CALL update_cauhoi_cautraloi(83, 329);

CALL insert_cauhoi(10, 'Quá trình phân tách tế bào gốc tạo ra các tế bào chuyên hóa được gọi là gì?');
CALL insert_cautraloi(84, 'Differentiation');
CALL insert_cautraloi(84, 'Apoptosis');
CALL insert_cautraloi(84, 'Mitosis');
CALL insert_cautraloi(84, 'Meiosis');
CALL update_cauhoi_cautraloi(84, 333);

CALL insert_cauhoi(10, 'Cấu trúc nào sau đây giúp tế bào động vật duy trì hình dạng và cấu trúc của mình?');
CALL insert_cautraloi(85, 'Cytoskeleton');
CALL insert_cautraloi(85, 'Nucleus');
CALL insert_cautraloi(85, ' Cell membrane');
CALL insert_cautraloi(85, 'Mitochondria');
CALL update_cauhoi_cautraloi(85, 337);

--chuong 11--

CALL insert_cauhoi(11, 'Cấu trúc tế bào thực vật khác biệt với tế bào động vật ở điểm nào sau đây?');
CALL insert_cautraloi(86, 'Thành tế bào');
CALL insert_cautraloi(86, 'Mitochondria');
CALL insert_cautraloi(86, 'Nucleus');
CALL insert_cautraloi(86, 'Thành tế bào bao phủ');
CALL update_cauhoi_cautraloi(86, 344);

CALL insert_cauhoi(11, 'Loại tế bào nào có khả năng tự tái tạo và chia để tạo ra các tế bào mới?');
CALL insert_cautraloi(87, 'Tế bào não');
CALL insert_cautraloi(87, 'Tế bào tim');
CALL insert_cautraloi(87, 'Tế bào gốc');
CALL insert_cautraloi(87, 'Tế bào cơ');
CALL update_cauhoi_cautraloi(87, 347);

CALL insert_cauhoi(11, 'Chức năng chính của mitochondria trong tế bào là gì?');
CALL insert_cautraloi(88, 'Tạo năng lượng');
CALL insert_cautraloi(88, 'Tổ chức cấu trúc tế bào');
CALL insert_cautraloi(88, 'Tạo RNA');
CALL insert_cautraloi(88, 'Tổng hợp protein');
CALL update_cauhoi_cautraloi(88, 349);

CALL insert_cauhoi(11, 'Quá trình phân tách tế bào gốc tạo ra các tế bào chuyên hóa được gọi là gì?');
CALL insert_cautraloi(89, 'Differentiation');
CALL insert_cautraloi(89, 'Apoptosis');
CALL insert_cautraloi(89, 'Mitosis');
CALL insert_cautraloi(89, 'Meiosis');
CALL update_cauhoi_cautraloi(89, 353);


CALL insert_cauhoi(11, 'Mitochondria là cấu trúc tế bào chịu trách nhiệm cho quá trình nào sau đây?');
CALL insert_cautraloi(90, ' Tạo năng lượng');
CALL insert_cautraloi(90, 'Tổng hợp protein');
CALL insert_cautraloi(90, ' Tạo RNA');
CALL insert_cautraloi(90, 'Sản xuất carbohydrate');
CALL update_cauhoi_cautraloi(90, 357);

CALL insert_cauhoi(11, 'Nơi nào trong tế bào đóng vai trò như là "bộ não" của tế bào?');
CALL insert_cautraloi(91, 'Ribosome');
CALL insert_cautraloi(91, 'Nucleus');
CALL insert_cautraloi(91, 'Endoplasmic reticulum');
CALL insert_cautraloi(91, 'Golgi apparatus');
CALL update_cauhoi_cautraloi(91, 362);

CALL insert_cauhoi(11, 'Quá trình tổng hợp protein diễn ra ở nơi nào trong tế bào?');
CALL insert_cautraloi(92, 'Ribosome');
CALL insert_cautraloi(92, 'Nucleus');
CALL insert_cautraloi(92, 'Endoplasmic reticulum');
CALL insert_cautraloi(92, 'Golgi apparatus');
CALL update_cauhoi_cautraloi(92, 365);

CALL insert_cauhoi(11, 'Cấu trúc nào chịu trách nhiệm vận chuyển các chất trong tế bào?');
CALL insert_cautraloi(93, 'Ribosome');
CALL insert_cautraloi(93, 'Nucleus');
CALL insert_cautraloi(93, 'Endoplasmic reticulum');
CALL insert_cautraloi(93, 'Golgi apparatus');
CALL update_cauhoi_cautraloi(93, 370);

CALL insert_cauhoi(11, 'Cấu trúc nào sau đây giúp tế bào động vật duy trì hình dạng và cấu trúc của mình?');
CALL insert_cautraloi(94, 'Cytoskeleton');
CALL insert_cautraloi(94, 'Nucleus');
CALL insert_cautraloi(94, 'Cell membrane');
CALL insert_cautraloi(94, 'Mitochondria');
CALL update_cauhoi_cautraloi(94, 373);

CALL insert_cauhoi(11, 'Quá trình nào chịu trách nhiệm cho việc tái tạo tế bào khi cần thiết?');
CALL insert_cautraloi(95, 'Mitosis');
CALL insert_cautraloi(95, 'Meiosis');
CALL insert_cautraloi(95, 'Apoptosis');
CALL insert_cautraloi(95, 'Cytokinesis');
CALL update_cauhoi_cautraloi(95, 377);

--chương 12--
CALL insert_cauhoi(12, 'Nơi nào trong tế bào chứa thông tin di truyền của một sinh vật?');
CALL insert_cautraloi(96, 'Ribosome');
CALL insert_cautraloi(96, 'Endoplasmic reticulum');
CALL insert_cautraloi(96, 'Nucleus');
CALL insert_cautraloi(96, 'Mitochondria');
CALL update_cauhoi_cautraloi(96, 383);

CALL insert_cauhoi(12, 'Loại nào sau đây của gen định hình các đặc tính bên ngoài của một sinh vật?');
CALL insert_cautraloi(97, 'Genotype');
CALL insert_cautraloi(97, 'Phenotype');
CALL insert_cautraloi(97, 'Allele');
CALL insert_cautraloi(97, 'Chromosome');
CALL update_cauhoi_cautraloi(97, 386);

CALL insert_cauhoi(12, 'Mendel đã làm việc với loài thực vật nào trong nghiên cứu về di truyền?');
CALL insert_cautraloi(98, 'Hành');
CALL insert_cautraloi(98, 'Bắp cải');
CALL insert_cautraloi(98, 'Đậu');
CALL insert_cautraloi(98, 'Bông');
CALL update_cauhoi_cautraloi(98, 391);

CALL insert_cauhoi(12, 'Đặc điểm nào sau đây Mendel sử dụng để nghiên cứu di truyền trong đậu?');
CALL insert_cautraloi(99, 'Màu hoa');
CALL insert_cautraloi(99, 'Chiều cao cây');
CALL insert_cautraloi(99, ' Màu hạt');
CALL insert_cautraloi(99, 'Kích thước lá');
CALL update_cauhoi_cautraloi(99, 395);

CALL insert_cauhoi(12, 'Cặp gen nào sau đây không tuân theo quy luật phân li kết hợp độc lập của Mendel?');
CALL insert_cautraloi(100, 'Allele liên kết');
CALL insert_cautraloi(100, 'Allele đôi một phân li');
CALL insert_cautraloi(100, 'Allele ở cùng một locus');
CALL insert_cautraloi(100, ' Allele ở các locus khác nhau');
CALL update_cauhoi_cautraloi(100, 397);

CALL insert_cauhoi(12, 'Hiện tượng nào mô tả sự xuất hiện của nhiều hơn hai bản sao của gen cụ thể trong một loài?');
CALL insert_cautraloi(101, 'Monosomy');
CALL insert_cautraloi(101, 'Trisomy');
CALL insert_cautraloi(101, 'Polysomy');
CALL insert_cautraloi(101, 'Polyploidy');
CALL update_cauhoi_cautraloi(101, 404);

CALL insert_cauhoi(12, 'Cấu trúc nào của DNA chứa thông tin di truyền của một sinh vật?');
CALL insert_cautraloi(102, 'Genotype');
CALL insert_cautraloi(102, 'Chromosome');
CALL insert_cautraloi(102, 'Allele');
CALL insert_cautraloi(102, 'Nucleotide');
CALL update_cauhoi_cautraloi(102, 406);

CALL insert_cauhoi(12, 'Quá trình nào dẫn đến sự tạo ra các tế bào con không giống với tế bào cha mẹ?');
CALL insert_cautraloi(103, 'Mitosis');
CALL insert_cautraloi(103, 'Meiosis');
CALL insert_cautraloi(103, 'Binary fission');
CALL insert_cautraloi(103, 'Budding');
CALL update_cauhoi_cautraloi(103, 410);

CALL insert_cauhoi(12, 'Đặc điểm nào sau đây không phụ thuộc vào gen?');
CALL insert_cautraloi(104, 'Màu da');
CALL insert_cautraloi(104, 'Chiều cao');
CALL insert_cautraloi(104, 'Nhóm máu');
CALL insert_cautraloi(104, 'Màu mắt');
CALL update_cauhoi_cautraloi(104, 414);

CALL insert_cauhoi(12, 'Quá trình di truyền gen có thể giải thích được thông qua quy luật nào sau đây được đề xuất bởi Mendel?');
CALL insert_cautraloi(105, 'Quy luật phân li kết hợp độc lập');
CALL insert_cautraloi(105, 'Quy luật phân li ngẫu nhiên');
CALL insert_cautraloi(105, 'Quy luật phân li liên kết');
CALL insert_cautraloi(105, 'Quy luật phân li trội và kém trội');
CALL update_cauhoi_cautraloi(105, 417);

--chương 13--

CALL insert_cauhoi(13, 'Quá trình nào sau đây là quá trình phân biệt tế bào tạo ra các loại tế bào chuyên biệt khác nhau trong cơ thể động vật?');
CALL insert_cautraloi(106, 'Mitosis');
CALL insert_cautraloi(106, 'Meiosis');
CALL insert_cautraloi(106, 'Differentiation');
CALL insert_cautraloi(106, 'Fertilization');
CALL update_cauhoi_cautraloi(106, 423);

CALL insert_cauhoi(13, 'Phân loại động vật dựa trên cấu trúc cơ thể được thực hiện như thế nào?');
CALL insert_cautraloi(107, 'Theo hình dáng');
CALL insert_cautraloi(107, 'Theo kích thước');
CALL insert_cautraloi(107, 'Theo hệ thống cơ quan');
CALL insert_cautraloi(107, 'Theo môi trường sống');
CALL update_cauhoi_cautraloi(107, 427);

CALL insert_cauhoi(13, 'Động vật nào sau đây không phải là động vật có vú?');
CALL insert_cautraloi(108, 'Chim');
CALL insert_cautraloi(108, 'Rùa');
CALL insert_cautraloi(108, 'Hải mã');
CALL insert_cautraloi(108, 'Cá voi');
CALL update_cauhoi_cautraloi(108, 432);

CALL insert_cauhoi(13, 'Quá trình nào sau đây dẫn đến việc tạo ra các gametes (tế bào sinh dục)?');
CALL insert_cautraloi(109, 'Meiosis');
CALL insert_cautraloi(109, 'Mitosis');
CALL insert_cautraloi(109, 'Fertilization ');
CALL insert_cautraloi(109, 'Differentiation');
CALL update_cauhoi_cautraloi(109, 433);

CALL insert_cauhoi(13, 'Hệ hô hấp của động vật bao gồm những cơ quan nào?');
CALL insert_cautraloi(110, 'Phổi, phế quản, túi khí');
CALL insert_cautraloi(110, 'Ruột non, ruột già, thực quản');
CALL insert_cautraloi(110, 'Dạ dày, gan, túi mật ');
CALL insert_cautraloi(110, 'Tim, huyết quản, phổi');
CALL update_cauhoi_cautraloi(110, 437);

CALL insert_cauhoi(13, 'Quá trình nào dẫn đến sự tăng trưởng và phát triển của động vật sau khi sinh?');
CALL insert_cautraloi(111, 'Mitosis');
CALL insert_cautraloi(111, 'Meiosis');
CALL insert_cautraloi(111, 'Development');
CALL insert_cautraloi(111, 'Growth');
CALL update_cauhoi_cautraloi(111, 443);

CALL insert_cauhoi(13, 'Động vật thuộc lớp nào có xương sống có vú?');
CALL insert_cautraloi(112, 'Lớp Cá');
CALL insert_cautraloi(112, 'Lớp Chim');
CALL insert_cautraloi(112, ' Lớp Thú');
CALL insert_cautraloi(112, 'Lớp Lưỡi trai');
CALL update_cauhoi_cautraloi(112, 447);

CALL insert_cauhoi(13, 'Nơi nào trong cơ thể động vật chịu trách nhiệm cho việc điều chỉnh các chức năng cơ bản như nhiệt độ cơ thể và giữ cân bằng nước?');
CALL insert_cautraloi(113, 'Phổi');
CALL insert_cautraloi(113, 'Thận');
CALL insert_cautraloi(113, 'Tuyến giáp');
CALL insert_cautraloi(113, 'Hệ thần kinh trung ương');
CALL update_cauhoi_cautraloi(113, 452);

CALL insert_cauhoi(13, 'Phân loại động vật theo loài dựa trên điểm gì?');
CALL insert_cautraloi(114, 'Phổi');
CALL insert_cautraloi(114, 'Thận');
CALL insert_cautraloi(114, 'Tuyến giáp');
CALL insert_cautraloi(114, 'Hệ thần kinh trung ương');
CALL update_cauhoi_cautraloi(114, 455);

CALL insert_cauhoi(13, 'Đặc điểm nào sau đây không phụ thuộc vào loài động vật mà phụ thuộc vào môi trường sống?');
CALL insert_cautraloi(115, ' Màu da');
CALL insert_cautraloi(115, 'Loại thức ăn');
CALL insert_cautraloi(115, 'Kích thước cơ thể');
CALL insert_cautraloi(115, 'Hệ thống tiêu hóa');
CALL update_cauhoi_cautraloi(115, 457);

--chương 14--

CALL insert_cauhoi(14, 'Sinh thái học nghiên cứu về gì?');
CALL insert_cautraloi(116, 'Các loài động vật');
CALL insert_cautraloi(116, 'Cơ chế di truyền');
CALL insert_cautraloi(116, 'Môi trường sống và mối quan hệ giữa các sinh vật với môi trường');
CALL insert_cautraloi(116, 'Sự tiến hóa của các loài');
CALL update_cauhoi_cautraloi(116, 463);

CALL insert_cauhoi(14, 'Khái niệm nào miêu tả sự tương tác giữa các sinh vật cùng loài sống chung trong một khu vực nhất định?');
CALL insert_cautraloi(117, 'Cộng đồng');
CALL insert_cautraloi(117, 'Hệ sinh thái');
CALL insert_cautraloi(117, 'Quần thể');
CALL insert_cautraloi(117, 'Sự đa dạng sinh học');
CALL update_cauhoi_cautraloi(117, 465);

CALL insert_cauhoi(14, 'Đơn vị cơ bản của sinh thái học được gọi là gì?');
CALL insert_cautraloi(118, 'Động vật');
CALL insert_cautraloi(118, 'Thực vật');
CALL insert_cautraloi(118, 'Sinh vật');
CALL insert_cautraloi(118, ' Hệ sinh thái');
CALL update_cauhoi_cautraloi(118, 472);

CALL insert_cauhoi(14, 'Sự phân bố đều đặn của các sinh vật trong một khu vực gọi là gì?');
CALL insert_cautraloi(119, 'Biến đổi khí hậu');
CALL insert_cautraloi(119, ' Phân bố đồng đều');
CALL insert_cautraloi(119, 'Phân bố rải rác');
CALL insert_cautraloi(119, 'Tương tác sinh thái');
CALL update_cauhoi_cautraloi(119, 474);

CALL insert_cauhoi(14, 'Sự phân bố không đồng đều của các sinh vật trong một khu vực gọi là gì?');
CALL insert_cautraloi(120, 'Biến đổi khí hậu');
CALL insert_cautraloi(120, ' Phân bố đồng đều');
CALL insert_cautraloi(120, 'Phân bố rải rác');
CALL insert_cautraloi(120, 'Tương tác sinh thái');
CALL update_cauhoi_cautraloi(120, 479);

CALL insert_cauhoi(14, 'Nơi nào trong hệ sinh thái đóng vai trò quan trọng nhất trong việc xử lý các chất thải hữu cơ?');
CALL insert_cautraloi(121, 'Sông, suối');
CALL insert_cautraloi(121, 'Rừng, rừng ngập mặn');
CALL insert_cautraloi(121, 'Cây cối');
CALL insert_cautraloi(121, ' Đất và vi khuẩn đất');
CALL update_cauhoi_cautraloi(121, 484);

CALL insert_cauhoi(14, 'Môi trường sống bao gồm yếu tố nào sau đây?');
CALL insert_cautraloi(122, ' Nhiệt độ, độ ẩm, ánh sáng');
CALL insert_cautraloi(122, 'Loại đất, môi trường nước');
CALL insert_cautraloi(122, 'Độ cao, khí hậu');
CALL insert_cautraloi(122, 'Tất cả đều đúng');
CALL update_cauhoi_cautraloi(122, 488);

CALL insert_cauhoi(14, 'Quá trình nào xác định sự phân bố các loài trong một cộng đồng sinh vật?');
CALL insert_cautraloi(123, 'Cạnh tranh giữa các loài');
CALL insert_cautraloi(123, 'Tương tác hợp tác');
CALL insert_cautraloi(123, 'Quá trình tiến hóa');
CALL insert_cautraloi(123, 'Sự tiêu hóa thức ăn');
CALL update_cauhoi_cautraloi(123, 489);

CALL insert_cauhoi(14, 'Sự biến đổi môi trường gây ảnh hưởng lớn đến sinh thái gọi là gì?');
CALL insert_cautraloi(124, 'Cảm biến môi trường');
CALL insert_cautraloi(124, 'Biến đổi môi trường');
CALL insert_cautraloi(124, 'Biến đổi khí hậu');
CALL insert_cautraloi(124, 'Biến đổi sinh học');
CALL update_cauhoi_cautraloi(124, 495);

CALL insert_cauhoi(14, 'Môi trường sống của một sinh vật cụ thể được gọi là gì?');
CALL insert_cautraloi(125, 'Hệ sinh thái');
CALL insert_cautraloi(125, 'Môi trường sống');
CALL insert_cautraloi(125, 'Cộng đồng');
CALL insert_cautraloi(125, 'Quần thể');
CALL update_cauhoi_cautraloi(125, 498);

/*Hóa học*/
--chuong15--
CALL insert_cauhoi(15, 'Nguyên tử là gì?');
CALL insert_cautraloi(126, 'Phân tử nhỏ nhất trong chất');
CALL insert_cautraloi(126, 'Đơn vị cấu tạo cơ bản của vật chất');
CALL insert_cautraloi(126, 'Các hạt nhỏ gồm proton và electron');
CALL insert_cautraloi(126, 'Đơn vị cấu trúc của các hợp chất hóa học');
CALL update_cauhoi_cautraloi(126, 502);

CALL insert_cauhoi(15, 'Khái niệm "khối lượng nguyên tử" được đo bằng gì?');
CALL insert_cautraloi(127, 'Đơn vị khối lượng nguyên tử');
CALL insert_cautraloi(127, 'Đơn vị khối lượng phân tử');
CALL insert_cautraloi(127, ' Đơn vị khối lượng hạt nhân');
CALL insert_cautraloi(127, 'Đơn vị khối lượng electron');
CALL update_cauhoi_cautraloi(127, 505);

CALL insert_cauhoi(15, 'Trong bảng tuần hoàn, các nguyên tố được sắp xếp dựa trên gì?');
CALL insert_cautraloi(128, 'Khối lượng nguyên tử tăng dần');
CALL insert_cautraloi(128, 'Số proton trong hạt nhân');
CALL insert_cautraloi(128, 'Số electron trong vỏ electron');
CALL insert_cautraloi(128, 'Số neutron');
CALL update_cauhoi_cautraloi(128, 510);

CALL insert_cauhoi(15, 'Công thức hóa học của nước là gì?');
CALL insert_cautraloi(129, 'H2O');
CALL insert_cautraloi(129, 'CO2');
CALL insert_cautraloi(129, 'CH4');
CALL insert_cautraloi(129, 'NaCl');
CALL update_cauhoi_cautraloi(129, 513);

CALL insert_cauhoi(15, 'Nguyên tố nào là thành phần chính của không khí?');
CALL insert_cautraloi(130, 'Oxy');
CALL insert_cautraloi(130, 'Nitơ');
CALL insert_cautraloi(130, 'Hydro');
CALL insert_cautraloi(130, 'Carbon');
CALL update_cauhoi_cautraloi(130, 518);

CALL insert_cauhoi(15, 'Hợp chất hóa học nào chứa liên kết ion?');
CALL insert_cautraloi(131, 'NaCl (muối)');
CALL insert_cautraloi(131, 'H2O (nước)');
CALL insert_cautraloi(131, 'CO2 (carbon dioxide)');
CALL insert_cautraloi(131, 'CH4 (methane)');
CALL update_cauhoi_cautraloi(131, 521);

CALL insert_cauhoi(15, 'Hợp chất hóa học nào chứa liên kết ion?');
CALL insert_cautraloi(132, 'Các nguyên tố trong tự nhiên');
CALL insert_cautraloi(132, 'Cấu trúc cơ bản của vật chất');
CALL insert_cautraloi(132, 'Liên kết giữa các nguyên tử');
CALL insert_cautraloi(132, 'Sự biến đổi của nguyên tử');
CALL update_cauhoi_cautraloi(132, 527);

CALL insert_cauhoi(15, 'Hợp chất hóa học nào chứa liên kết ion?');
CALL insert_cautraloi(133, 'Có, luôn thay đổi');
CALL insert_cautraloi(133, 'Không, không bao giờ thay đổi');
CALL insert_cautraloi(133, 'Thay đổi tuỳ theo loại phản ứng');
CALL insert_cautraloi(133, 'Đôi khi thay đổi');
CALL update_cauhoi_cautraloi(133, 530);

CALL insert_cauhoi(15, 'Hợp chất hóa học nào chứa liên kết ion?');
CALL insert_cautraloi(134, 'Quá trình mất electron');
CALL insert_cautraloi(134, 'Quá trình giảm proton');
CALL insert_cautraloi(134, ' Quá trình tăng khối lượng nguyên tử');
CALL insert_cautraloi(134, 'Quá trình thay đổi màu sắc');
CALL update_cauhoi_cautraloi(134, 533);

CALL insert_cauhoi(15, 'Hợp chất hóa học nào chứa liên kết ion?');
CALL insert_cautraloi(135, 'Oxy');
CALL insert_cautraloi(135, 'Silic');
CALL insert_cautraloi(135, 'Nhôm');
CALL insert_cautraloi(135, 'Sắt');
CALL update_cauhoi_cautraloi(135, 537);

--chuong16--
CALL insert_cauhoi(16, 'Nguyên tử của một nguyên tố có thể được xác định chủ yếu thông qua điều gì?');
CALL insert_cautraloi(136, 'Số electronxy');
CALL insert_cautraloi(136, 'Số proton');
CALL insert_cautraloi(136, ' Số neutron');
CALL insert_cautraloi(136, 'Khối lượng nguyên tử');
CALL update_cauhoi_cautraloi(136, 542);

CALL insert_cauhoi(16, 'Electronegativity đo lường điều gì về một nguyên tố?');
CALL insert_cautraloi(137, 'Khả năng thu hút electron trong một liên kết hóa học');
CALL insert_cautraloi(137, 'Số electron trong vỏ electron');
CALL insert_cautraloi(137, 'Độ âm điện của proton');
CALL insert_cautraloi(137, 'Sự phân cực của nguyên tử');
CALL update_cauhoi_cautraloi(137, 545);

CALL insert_cauhoi(16, 'Trong bảng tuần hoàn, tính chất hóa học của các nguyên tố thay đổi như thế nào?');
CALL insert_cautraloi(138, 'Theo chiều từ trên xuống dưới');
CALL insert_cautraloi(138, 'Theo chiều từ trái sang phải');
CALL insert_cautraloi(138, 'Không có sự thay đổi');
CALL insert_cautraloi(138, 'Theo chiều ngẫu nhiên');
CALL update_cauhoi_cautraloi(138, 550);

CALL insert_cauhoi(16, 'Loại nguyên tử nào làm cho nguyên tố trở thành isotop?');
CALL insert_cautraloi(139, 'Có số proton khác nhau');
CALL insert_cautraloi(139, 'Có số electron khác nhau');
CALL insert_cautraloi(139, 'Có số neutron khác nhau');
CALL insert_cautraloi(139, ' Có cả số proton và neutron khác nhau');
CALL update_cauhoi_cautraloi(139, 555);

CALL insert_cauhoi(16, 'Trong nguyên tử, electron được tìm thấy ở đâu?');
CALL insert_cautraloi(140, 'Trong hạt nhân');
CALL insert_cautraloi(140, 'Xung quanh hạt nhân');
CALL insert_cautraloi(140, 'Trong vỏ electron');
CALL insert_cautraloi(140, 'Tại quỹ đạo nguyên tử');
CALL update_cauhoi_cautraloi(140, 559);

CALL insert_cauhoi(16, 'Số lượng ngoại lớp electron của một nguyên tử xác định điều gì?');
CALL insert_cautraloi(141, 'Hoá trị của nguyên tử');
CALL insert_cautraloi(141, 'Khối lượng nguyên tử');
CALL insert_cautraloi(141, 'Độ âm điện');
CALL insert_cautraloi(141, 'Bán kính nguyên tử');
CALL update_cauhoi_cautraloi(141, 561);

CALL insert_cauhoi(16, 'Nguyên tố nào chiếm tỷ lệ lớn nhất trong vỏ đất quảng?');
CALL insert_cautraloi(142, 'Oxy');
CALL insert_cautraloi(142, 'Silic');
CALL insert_cautraloi(142, 'Nhôm');
CALL insert_cautraloi(142, 'Sắt');
CALL update_cauhoi_cautraloi(142, 565);

CALL insert_cauhoi(16, 'Liên kết hóa học nào xảy ra khi electron chia sẻ hoàn toàn giữa các nguyên tử?');
CALL insert_cautraloi(143, ' Liên kết ion');
CALL insert_cautraloi(143, 'Liên kết cộng hóa trị');
CALL insert_cautraloi(143, 'Liên kết kim loại');
CALL insert_cautraloi(143, ' Liên kết cộng hóa trị đa chung');
CALL update_cauhoi_cautraloi(143, 570);

CALL insert_cauhoi(16, 'Sự phân cực trong liên kết hóa học liên quan đến điều gì?');
CALL insert_cautraloi(144, 'Sự chia sẻ electron không đều giữa các nguyên tử');
CALL insert_cautraloi(144, 'Sự chia sẻ electron hoàn toàn giữa các nguyên tử');
CALL insert_cautraloi(144, 'Sự chuyển giao electron giữa các nguyên tử');
CALL insert_cautraloi(144, 'Sự chuyển giao proton giữa các nguyên tử');
CALL update_cauhoi_cautraloi(144, 573);

CALL insert_cauhoi(16, 'Bán kính nguyên tử giảm dần theo chiều nào trong bảng tuần hoàn?');
CALL insert_cautraloi(145, 'Từ trên xuống dưới');
CALL insert_cautraloi(145, 'Từ trái sang phải');
CALL insert_cautraloi(145, ' Từ dưới lên trên');
CALL insert_cautraloi(145, ' Không có sự thay đổi');
CALL update_cauhoi_cautraloi(145, 578);

--chuong17--
CALL insert_cauhoi(17, 'Proton và neutron cùng tạo thành điều gì trong hạt nhân?');
CALL insert_cautraloi(146, 'Hạt beta');
CALL insert_cautraloi(146, 'Hạt alpha');
CALL insert_cautraloi(146, 'Hạt gamma');
CALL insert_cautraloi(146, 'Hạt nơtron');
CALL update_cauhoi_cautraloi(146, 582);

CALL insert_cauhoi(17, 'Số neutron và proton có liên quan đến gì trong hạt nhân ổn định?');
CALL insert_cautraloi(147, 'Bằng nhau');
CALL insert_cautraloi(147, ' Số proton lớn hơn số neutron');
CALL insert_cautraloi(147, 'Số neutron lớn hơn số proton');
CALL insert_cautraloi(147, 'Không có quan hệ');
CALL update_cauhoi_cautraloi(147, 585);

CALL insert_cauhoi(17, 'Quá trình nào sau đây không phải là quá trình phân rã hạt nhân?');
CALL insert_cautraloi(148, 'Alpha decay');
CALL insert_cautraloi(148, 'Beta decay');
CALL insert_cautraloi(148, 'Gamma decay');
CALL insert_cautraloi(148, 'Fusion');
CALL update_cauhoi_cautraloi(148, 592);

CALL insert_cauhoi(17, 'Cân bằng hạt nhân trong nguyên tử do điều gì?');
CALL insert_cautraloi(149, 'Sự cân bằng giữa proton và neutron');
CALL insert_cautraloi(149, 'Sự cân bằng giữa electron và proton');
CALL insert_cautraloi(149, 'Sự cân bằng giữa electron và neutron');
CALL insert_cautraloi(149, 'Sự cân bằng giữa proton và electron');
CALL update_cauhoi_cautraloi(149, 593);

CALL insert_cauhoi(17, 'Sự chuyển đổi neutron thành proton trong hạt nhân gọi là gì?');
CALL insert_cautraloi(150, 'Beta decay');
CALL insert_cautraloi(150, 'Alpha decay');
CALL insert_cautraloi(150, ' Gamma decay');
CALL insert_cautraloi(150, 'Neutron decay');
CALL update_cauhoi_cautraloi(150, 597);

CALL insert_cauhoi(17, 'Phản ứng hạt nhân nào sản sinh ra năng lượng mạnh nhất?');
CALL insert_cautraloi(151, 'Phản ứng hạt nhân hợp nhất');
CALL insert_cautraloi(151, 'Phản ứng hạt nhân vỡ vụn');
CALL insert_cautraloi(151, 'Phản ứng hạt nhân alpha');
CALL insert_cautraloi(151, 'Phản ứng hạt nhân beta');
CALL update_cauhoi_cautraloi(151, 601);

CALL insert_cauhoi(17, 'Nguyên tử nào có khả năng phân rã tự nhiên mà không cần kích thích từ bên ngoài?');
CALL insert_cautraloi(152, 'Nguyên tử không ổn định');
CALL insert_cautraloi(152, 'Nguyên tử ổn định');
CALL insert_cautraloi(152, 'Nguyên tử có năng lượng cao');
CALL insert_cautraloi(152, 'Nguyên tử có electron ngoại cùng ít nhất');
CALL update_cauhoi_cautraloi(152, 605);

CALL insert_cauhoi(17, 'Loại phản ứng hạt nhân nào phát ra tia gamma?');
CALL insert_cautraloi(153, 'Alpha decay');
CALL insert_cautraloi(153, 'Beta decay');
CALL insert_cautraloi(153, 'Gamma decay');
CALL insert_cautraloi(153, 'Fusion');
CALL update_cauhoi_cautraloi(153, 611);

CALL insert_cauhoi(17, 'Phản ứng hạt nhân nào có thể tạo ra nguyên tử mới hoặc cung cấp năng lượng trong sao chổi?');
CALL insert_cautraloi(154, 'Alpha decay');
CALL insert_cautraloi(154, 'Beta decay');
CALL insert_cautraloi(154, 'Fusion');
CALL insert_cautraloi(154, 'Gamma decay');
CALL update_cauhoi_cautraloi(154, 615);

CALL insert_cauhoi(17, 'Khái niệm "năng lượng hạt nhân" liên quan đến điều gì?');
CALL insert_cautraloi(155, 'Sự phân rã nguyên tử');
CALL insert_cautraloi(155, 'Năng lượng được giải phóng trong quá trình phân rã hạt nhân');
CALL insert_cautraloi(155, 'Năng lượng cung cấp cho electron trong vỏ electron');
CALL insert_cautraloi(155, 'Sự biến đổi của proton thành neutron');
CALL update_cauhoi_cautraloi(155, 618);

--chuong18--
CALL insert_cauhoi(18, 'Nguyên tố nào chiếm tỷ lệ lớn nhất trong mỏ quặng sắt (Fe)?');
CALL insert_cautraloi(156, 'Sắt (Iron)');
CALL insert_cautraloi(156, 'Oxy (Oxygen)');
CALL insert_cautraloi(156, 'Silic (Silicon)');
CALL insert_cautraloi(156, 'Carbon (Carbon)');
CALL update_cauhoi_cautraloi(156, 621);

CALL insert_cauhoi(18, 'Loại nguyên tố nào chiếm tỷ lệ lớn nhất trong phần trăm khối lượng của vỏ trái đất?');
CALL insert_cautraloi(157, 'Kim loại kiềm (Alkali metals)');
CALL insert_cautraloi(157, 'Kim loại kiềm thổ (Alkaline earth metals)');
CALL insert_cautraloi(157, 'Kim loại chuyển tiếp (Transition metals)');
CALL insert_cautraloi(157, 'Phi kim (Nonmetals)');
CALL update_cauhoi_cautraloi(157, 627);

CALL insert_cauhoi(18, 'Nguyên tố nào tạo ra hợp chất nước (H2O) khi kết hợp với oxy (O)?');
CALL insert_cautraloi(158, 'Hydro (Hydrogen)');
CALL insert_cautraloi(158, 'Nitơ (Nitrogen)');
CALL insert_cautraloi(158, 'Sắt (Iron)');
CALL insert_cautraloi(158, 'Kali (Potassium)');
CALL update_cauhoi_cautraloi(158, 629);

CALL insert_cauhoi(18, 'Công thức hóa học của muối bạc nitrat là gì?');
CALL insert_cautraloi(159, 'AgNO3');
CALL insert_cautraloi(159, 'AgCl');
CALL insert_cautraloi(159, 'Ag2SO4');
CALL insert_cautraloi(159, 'Ag2CO3');
CALL update_cauhoi_cautraloi(159, 633);

CALL insert_cauhoi(18, 'Loại phản ứng nào xảy ra khi hai chất giao dịch electron để tạo thành liên kết hóa học?');
CALL insert_cautraloi(160, 'Phản ứng cộng');
CALL insert_cautraloi(160, ' Phản ứng trao đổi');
CALL insert_cautraloi(160, ' Phản ứng oxi hóa khử');
CALL insert_cautraloi(160, ' Phản ứng trùng hợp');
CALL update_cauhoi_cautraloi(160, 638);

CALL insert_cauhoi(18, 'Kim loại nào sau đây không phản ứng với axit loãng?');
CALL insert_cautraloi(161, 'Kali (Potassium)');
CALL insert_cautraloi(161, 'Sắt (Iron)');
CALL insert_cautraloi(161, ' Nhôm (Aluminum)');
CALL insert_cautraloi(161, 'Magiê (Magnesium)');
CALL update_cauhoi_cautraloi(161, 643);

CALL insert_cauhoi(18, 'Nguyên tố nào chiếm tỷ lệ lớn nhất trong mỏ quặng đồng (Cu)?');
CALL insert_cautraloi(162, 'Sắt (Iron)');
CALL insert_cautraloi(162, 'Oxy (Oxygen)');
CALL insert_cautraloi(162, 'Silic (Silicon)');
CALL insert_cautraloi(162, 'Lưu huỳnh (Sulfur)');
CALL update_cauhoi_cautraloi(162, 646);

CALL insert_cauhoi(18, 'Hợp chất nào được sử dụng rộng rãi trong việc bôi trơn và làm mịn các bề mặt kim loại?');
CALL insert_cautraloi(163, 'H2SO4 (Axít sulfuric)');
CALL insert_cautraloi(163, ' NaCl (Muối)');
CALL insert_cautraloi(163, 'HCl (Axít clohidric)');
CALL insert_cautraloi(163, 'NaOH (Axit axetic)');
CALL update_cauhoi_cautraloi(163, 652);

CALL insert_cauhoi(18, 'Nguyên tố nào có thể được tìm thấy trong dạ dày người sống?');
CALL insert_cautraloi(164, 'Oxy (Oxygen)');
CALL insert_cautraloi(164, 'Hydro (Hydrogen)');
CALL insert_cautraloi(164, 'Nhôm (Aluminum)');
CALL insert_cautraloi(164, ' Sắt (Iron)');
CALL update_cauhoi_cautraloi(164, 656);

CALL insert_cauhoi(18, 'Hợp chất nào thường được sử dụng làm chất bảo quản gỗ?');
CALL insert_cautraloi(165, 'H2O2 (Nước oxy)');
CALL insert_cautraloi(165, 'NaOH (Axit axetic)');
CALL insert_cautraloi(165, 'CuSO4 (Sắt sunfat)');
CALL insert_cautraloi(165, 'NaCl (Muối)');
CALL update_cauhoi_cautraloi(165, 659);

--chuong19--

CALL insert_cauhoi(19, 'Một trong những đặc điểm cơ bản của hợp chất hữu cơ là gì?');
CALL insert_cautraloi(166, ' Chứa carbon');
CALL insert_cautraloi(166, ' Chứa hydrogen');
CALL insert_cautraloi(166, 'Chứa oxygen');
CALL insert_cautraloi(166, 'Chứa nitrogen');
CALL update_cauhoi_cautraloi(166, 661);

CALL insert_cauhoi(19, 'Cấu tạo cơ bản của các hợp chất hữu cơ gồm những nguyên tố nào?');
CALL insert_cautraloi(167, 'Carbon và nitrogen');
CALL insert_cautraloi(167, 'Carbon và oxygen');
CALL insert_cautraloi(167, 'Carbon và hydrogen');
CALL insert_cautraloi(167, 'Carbon và sulfur');
CALL update_cauhoi_cautraloi(167, 667);

CALL insert_cauhoi(19, 'Khái niệm "cácbon không no" (unsaturated carbon) thường ám chỉ điều gì?');
CALL insert_cautraloi(168, 'Cácbon có khối lượng lớn hơn');
CALL insert_cautraloi(168, 'Cácbon liên kết với các nguyên tử khác nhau');
CALL insert_cautraloi(168, 'Cácbon không đủ electron');
CALL insert_cautraloi(168, 'Cácbon có liên kết đôi hoặc ba');
CALL update_cauhoi_cautraloi(168, 672);

CALL insert_cauhoi(19, 'Trong hợp chất hữu cơ, liên kết đôi thường xảy ra giữa nguyên tử nào?');
CALL insert_cautraloi(169, 'Carbon và oxygen');
CALL insert_cautraloi(169, 'Carbon và hydrogen');
CALL insert_cautraloi(169, 'Carbon và nitrogen');
CALL insert_cautraloi(169, 'Carbon và carbon');
CALL update_cauhoi_cautraloi(169, 676);

CALL insert_cauhoi(19, 'Hợp chất nào sau đây không thuộc về nhóm hợp chất hữu cơ?');
CALL insert_cautraloi(170, 'H2O');
CALL insert_cautraloi(170, 'CH4');
CALL insert_cautraloi(170, 'C6H12O6');
CALL insert_cautraloi(170, 'C2H5OH');
CALL update_cauhoi_cautraloi(170, 677);

CALL insert_cauhoi(19, 'Một nhóm chức (functional group) là gì trong hóa học hữu cơ?');
CALL insert_cautraloi(171, 'Nhóm nguyên tử tạo thành một phân tử');
CALL insert_cautraloi(171, 'Nhóm nguyên tử đóng vai trò quan trọng trong tính chất hóa học của một phân tử');
CALL insert_cautraloi(171, 'Nhóm nguyên tử chỉ định khối lượng của một phân tử');
CALL insert_cautraloi(171, 'Nhóm nguyên tử chỉ định hình dạng của một phân tử');
CALL update_cauhoi_cautraloi(171, 682);

CALL insert_cauhoi(19, 'Hợp chất nào dưới đây có chứa nhóm chức hydroxyl (-OH)?');
CALL insert_cautraloi(172, 'Methanol');
CALL insert_cautraloi(172, 'Ethane');
CALL insert_cautraloi(172, 'Propane');
CALL insert_cautraloi(172, 'Ethanol');
CALL update_cauhoi_cautraloi(172, 688);

CALL insert_cauhoi(19, 'Nhóm chức carbonyl có thành phần gồm nguyên tử nào?');
CALL insert_cautraloi(173, 'C=O');
CALL insert_cautraloi(173, 'C-OH');
CALL insert_cautraloi(173, 'C-O-C');
CALL insert_cautraloi(173, 'C=C');
CALL update_cauhoi_cautraloi(173, 689);

CALL insert_cauhoi(19, 'Nhóm chức carboxyl thường được tìm thấy ở đâu trong các hợp chất hữu cơ?');
CALL insert_cautraloi(174, 'Aldehydes');
CALL insert_cautraloi(174, 'Acids');
CALL insert_cautraloi(174, 'Alcohols');
CALL insert_cautraloi(174, 'Ethers');
CALL update_cauhoi_cautraloi(174, 694);

CALL insert_cauhoi(19, 'Hợp chất nào chứa nhóm chức amino (-NH2)?');
CALL insert_cautraloi(175, 'Amines');
CALL insert_cautraloi(175, 'Aldehydes');
CALL insert_cautraloi(175, 'Alcohols');
CALL insert_cautraloi(175, 'Carboxylic acids');
CALL update_cauhoi_cautraloi(175, 697);

-- Quyền
CALL insert_quyen('admin');
CALL insert_quyen('người dùng thường');

-- Người dùng
CALL insert_nguoidung('admin', 1, '$2a$10$wpVohDQPVeUNlVIvAePAvuKr0WgoDLShl7oFHxz5HBmuBOJz1ijhK', 'Admin toàn năng');
CALL insert_nguoidung('khang', 2, '$2a$10$6jDhbTlbwS5vCDSXA55Ftu4SPNnr/0SA1XVAC/CqFOt9gsSiiL6Zm', 'Chỉ là khang phế vật');

-- Sinh viên
CALL insert_sinhvien('admin', 'Admin tester', 'Nam', '2000-12-25 00:00:00');
CALL insert_sinhvien('khang', 'Khang', 'Nam', '2003-12-25 00:00:00');

CALL insert_dangkylophoc(1, 1);

-- Random dethi
CALL insert_dethimonhoc(1, 'Lịch sử 5p', false, 10, 5);
CALL insert_dethimonhoc(2, 'Địa lý 5p', false, 10, 5);
CALL insert_dethimonhoc(2, 'Địa lý 10p', true, 20, 10);

-- Lưu ý là set thời gian tương lai để thực hiện insert_dethichinh_random thành công
-- CALL insert_thoigiandethichinh(3, '2023-12-12 08:05:00');

CALL insert_dethithu_random(1, 1);

-- CALL insert_dethichinh_random(3, 1);

CALL insert_dangkylophoc(1, 1);
CALL update_dangkylophoc(1, 1, 2);
CALL insert_dangkylophoc(2, 1);
