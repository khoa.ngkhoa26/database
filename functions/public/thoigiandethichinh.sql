/* Lấy dữ liệu thoigiandethichinh */
CREATE OR REPLACE FUNCTION getall_thoigiandethichinh_withlimitoffset(
    p_limit INT,
    p_offset INT
)
RETURNS TABLE (
    thoigiandethichinh_id   INT,
    dethimonhoc_id          INT,
    thoigiandethichinh_time TIMESTAMP WITH TIME ZONE
) AS
$$
DECLARE
    thoigiandethichinh_count INT;
BEGIN
    SELECT COUNT(tg.thoigiandethichinh_id) INTO thoigiandethichinh_count FROM thoigiandethichinh tg;

    IF p_limit < 5 THEN
        p_limit := 5;
    ELSIF p_limit > 50 THEN
        p_limit := 50;
    END IF;

    IF p_offset < 0 THEN
        p_offset := 0;
    ELSIF p_offset >= CEIL(thoigiandethichinh_count::NUMERIC / p_limit) THEN
        p_offset := CASE WHEN CEIL(thoigiandethichinh_count::NUMERIC / p_limit) > 0
                         THEN CEIL(thoigiandethichinh_count::NUMERIC / p_limit) - 1
                         ELSE 0
                    END;
    END IF;

    RETURN QUERY
        SELECT
            tg.thoigiandethichinh_id,
            tg.dethimonhoc_id,
            tg.thoigiandethichinh_time
        FROM thoigiandethichinh tg
        LIMIT p_limit
        OFFSET p_offset*p_limit;
END;
$$
LANGUAGE plpgsql;

/* Lấy thời gian đế thi chính theo sinh viên */
CREATE OR REPLACE FUNCTION getall_thoigiandethichinh_on_sinhvien(
    p_sinhvien_id INT
)
RETURNS TABLE (
    thoigiandethichinh_id   INT,
    dethimonhoc_id          INT,
    thoigiandethichinh_time TIMESTAMP WITH TIME ZONE
) AS
$$
BEGIN
    RETURN QUERY
        SELECT
            tg.thoigiandethichinh_id,
            tg.dethimonhoc_id,
            tg.thoigiandethichinh_time
        FROM thoigiandethichinh tg
        INNER JOIN dethimonhoc dtmh ON tg.dethimonhoc_id = dtmh.dethimonhoc_id
        INNER JOIN monhoc mh ON dtmh.monhoc_id = mh.monhoc_id
        INNER JOIN lophoc lh ON mh.lophoc_id = lh.lophoc_id
        INNER JOIN dangkylophoc dklh ON lh.lophoc_id = dklh.lophoc_id
        INNER JOIN sinhvien sv ON dklh.sinhvien_id = sv.sinhvien_id
        WHERE sv.sinhvien_id = p_sinhvien_id;
END;
$$
LANGUAGE plpgsql;

/* Đếm số thoigiandethichinh */
CREATE OR REPLACE FUNCTION getcount_thoigiandethichinh()
RETURNS INT AS
$$
DECLARE
    thoigiandethichinh_count INT;
BEGIN
    SELECT COUNT(tgdtc.thoigiandethichinh_id) FROM thoigiandethichinh tgdtc;

    RETURN thoigiandethichinh_count;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insert_thoigiandethichinh_if_dethimonhocreal()
RETURNS TRIGGER AS $$
DECLARE
    value_real BOOLEAN;
BEGIN
    -- Lấy giá trị dethimonhoc_real để kiểm tra có phải đề thi chính thức không
    SELECT dtmh.dethimonhoc_real INTO value_real FROM dethimonhoc dtmh
    WHERE dtmh.dethimonhoc_id = NEW.dethimonhoc_id;

    -- So sánh real có false không
    IF (value_real = false) THEN
        -- Nếu có bất kỳ sự cố gắng INSERT nào, raise một exception
        RAISE EXCEPTION 'Inserting dethimonhoc table is not allowed if dethimonhoc_real is not true';
        RETURN NULL; -- Trả về NULL để hủy bỏ thực hiện INSERT
    ELSE
        RETURN NEW;
    END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION update_thoigiandethichinh_if_dethimonhocreal()
RETURNS TRIGGER AS $$
DECLARE
    value_real BOOLEAN;
BEGIN
    -- Lấy giá trị đdethimonhoc_real
    SELECT dtmh.dethimonhoc_real INTO value_real FROM dethimonhoc dtmh
    WHERE dtmh.dethimonhoc_id = NEW.dethimonhoc_id;

    -- So sánh real có false không
    IF (value_real = false) THEN
        -- Nếu có bất kỳ sự cố gắng INSERT nào, raise một exception
        RAISE EXCEPTION 'Updating dethimonhoc table is not allowed if dethimonhoc_real is not true';
        RETURN NULL; -- Trả về NULL để hủy bỏ thực hiện INSERT
    ELSE
        RETURN NEW;
    END IF;
END;
$$ LANGUAGE plpgsql;