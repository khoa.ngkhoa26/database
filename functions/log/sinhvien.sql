/* Nhập dữ liệu cho bảng log.sinhvien */
CREATE OR REPLACE FUNCTION insert_logsinhvien()
RETURNS TRIGGER AS
$$
DECLARE
    public_count INT;
    public_last  INT;
    value_last   INT;
BEGIN
    -- Lấy vị trí cuối của bảng public.sinhvien
    SELECT LAST_VALUE(public.sinhvien.sinhvien_id)
        OVER (ORDER BY public.sinhvien.sinhvien_id DESC)
        INTO public_last FROM public.sinhvien;

    -- Lấy giá trị cuối của public.sinhvien
    SELECT LAST_VALUE(log.sinhvien.sinhvien_id) OVER
        (ORDER BY log.sinhvien.sinhvien_id DESC)
        INTO value_last FROM log.sinhvien;

    -- So sánh giá trị tổng và vị trí cuối nếu giá trị tổng bé hơn
    IF (OLD.sinhvien_id < public_last) THEN
      -- Thêm dữ liệu
      IF (value_last >= 1) THEN
          INSERT INTO log.sinhvien (sinhvien_id, public_sinhvien_id)
              VALUES (value_last+1, OLD.sinhvien_id);
      ELSE
          INSERT INTO log.sinhvien (sinhvien_id, public_sinhvien_id)
              VALUES (1, OLD.sinhvien_id);
      END IF;
    END IF;

    RETURN OLD;
END;
$$
LANGUAGE plpgsql;

/* Xóa dữ liệu cho bảng log.sinhvien */
CREATE OR REPLACE FUNCTION delete_logsinhvien()
RETURNS TRIGGER AS
$$
DECLARE
    log_count            INT;
    log_public_sinhvien_id INT;
BEGIN
    SELECT COUNT(log.sinhvien.sinhvien_id) INTO log_count FROM log.sinhvien;

    IF (log_count > 0) THEN
        SELECT public.sinhvien.sinhvien_id INTO log_public_sinhvien_id
            FROM public.sinhvien
            INNER JOIN log.sinhvien ON public.sinhvien.sinhvien_id = log.sinhvien.public_sinhvien_id
            ORDER BY public.sinhvien.sinhvien_id DESC LIMIT 1;

        DELETE FROM log.sinhvien WHERE log.sinhvien.public_sinhvien_id = log_public_sinhvien_id;
    END IF;

    RETURN NEW;
END;
$$
LANGUAGE plpgsql;