/* Insert */
CREATE OR REPLACE PROCEDURE insert_lophoc(
    p_name  TEXT
) AS
$$
DECLARE
    log_count  INT;
    log_array  INT[] := ARRAY[]::INT[];
    value_last INT;
    item       INT;
BEGIN
    -- Lấy tổng giá trị trong bảng log.lophoc
    SELECT COUNT(log.lophoc.lophoc_id) INTO log_count FROM log.lophoc;

    -- Nếu tổng giá trị trong bảng log.lophoc lớn hơn thì thêm với id từ bảng
    IF (log_count > 0) THEN
        log_array := ARRAY (
            SELECT log.lophoc.public_lophoc_id FROM log.lophoc
                ORDER BY log.lophoc.public_lophoc_id ASC LIMIT 1
        );

        item := log_array[1];

        -- Thêm dữ liệu
        INSERT INTO lophoc (lophoc_id, lophoc_name)
            VALUES (item, p_name);
    ELSE
        -- Lấy vị trí cuối
        SELECT LAST_VALUE(lophoc_id) OVER (ORDER BY lophoc_id DESC)
            INTO value_last FROM lophoc;

        -- Thêm dữ liệu
        IF (value_last >= 1) THEN
            INSERT INTO lophoc (lophoc_id, lophoc_name)
                VALUES (value_last+1, p_name);
        ELSE
            INSERT INTO lophoc (lophoc_id, lophoc_name)
                VALUES (1, p_name);
        END IF;
    END IF;
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_lophoc(
    p_id    INT,
    p_name  TEXT
) AS
$$
BEGIN
    UPDATE lophoc
    SET lophoc_name = p_name
    WHERE lophoc_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Delete */
CREATE OR REPLACE PROCEDURE delete_lophoc(p_id INT)
AS
$$
BEGIN
    DELETE FROM lophoc
    WHERE lophoc_id = p_id;
END;
$$
LANGUAGE plpgsql;