/* Insert */
CREATE OR REPLACE PROCEDURE insert_thoigiandethichinh(
    p_dethimonhoc_id INT,
    p_time           TIMESTAMP WITH TIME ZONE
) AS
$$
DECLARE
    log_count  INT;
    log_array  INT[] := ARRAY[]::INT[];
    value_last INT;
    item       INT;
BEGIN
    -- Lấy tổng giá trị trong bảng log.thoigiandethichinh
    SELECT COUNT(log.thoigiandethichinh.thoigiandethichinh_id) INTO log_count FROM log.thoigiandethichinh;

    -- Nếu tổng giá trị trong bảng log.thoigiandethichinh lớn hơn thì thêm với id từ bảng
    IF (log_count > 0) THEN
        log_array := ARRAY (
            SELECT log.thoigiandethichinh.public_thoigiandethichinh_id FROM log.thoigiandethichinh
                ORDER BY log.thoigiandethichinh.public_thoigiandethichinh_id ASC LIMIT 1
        );

        item := log_array[1];

        -- Thêm dữ liệu
        INSERT INTO thoigiandethichinh (
            thoigiandethichinh_id,
            dethimonhoc_id,
            thoigiandethichinh_time
        ) VALUES (item, p_dethimonhoc_id, p_time);
    ELSE
        -- Lấy vị trí cuối
        SELECT LAST_VALUE(thoigiandethichinh_id) OVER (ORDER BY thoigiandethichinh_id DESC)
            INTO value_last FROM thoigiandethichinh;

        -- Thêm dữ liệu
        IF (value_last >= 1) THEN
            INSERT INTO thoigiandethichinh (
                thoigiandethichinh_id,
                dethimonhoc_id,
                thoigiandethichinh_time
            ) VALUES (value_last+1, p_dethimonhoc_id, p_time);
        ELSE
            INSERT INTO thoigiandethichinh (
                thoigiandethichinh_id,
                dethimonhoc_id,
                thoigiandethichinh_time
            ) VALUES (1, p_dethimonhoc_id, p_time);
        END IF;
    END IF;
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_thoigiandethichinh(
    p_id             INT,
    p_dethimonhoc_id INT,
    p_time           TIMESTAMP WITH TIME ZONE
) AS
$$
BEGIN
    UPDATE thoigiandethichinh
    SET dethimonhoc_id = p_dethimonhoc_id,
        thoigiandethichinh_time = p_time
    WHERE thoigiandethichinh_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Xóa thoigiandethichinh */
CREATE OR REPLACE PROCEDURE delete_thoigiandethichinh(
    p_id INT
)
AS
$$
BEGIN
    DELETE FROM thoigiandethichinh
    WHERE thoigiandethichinh_id = p_id;
END;
$$
LANGUAGE plpgsql;