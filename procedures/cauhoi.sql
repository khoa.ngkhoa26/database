/* Thêm vào bảng CauHoi (cần bổ sung trigger khi thêm xóa bảng) */
CREATE OR REPLACE PROCEDURE insert_cauhoi(
    p_chuong_id INT,
    p_name      TEXT
) AS
$$
DECLARE
    log_count  INT;
    log_array  INT[] := ARRAY[]::INT[];
    value_last INT;
    item       INT;
BEGIN
    -- Lấy tổng giá trị trong bảng log.cauhoi
    SELECT COUNT(log.cauhoi.cauhoi_id) INTO log_count FROM log.cauhoi;

    -- Nếu tổng giá trị trong bảng log.cauhoi lớn hơn thì thêm với id từ bảng
    IF (log_count > 0) THEN
        log_array := ARRAY (
            SELECT log.cauhoi.public_cauhoi_id FROM log.cauhoi
                ORDER BY log.cauhoi.public_cauhoi_id ASC LIMIT 1
        );

        item := log_array[1];

        -- Thêm dữ liệu
        INSERT INTO cauhoi (cauhoi_id, chuong_id, cauhoi_name)
            VALUES (item, p_chuong_id, p_name);
    ELSE
        -- Lấy vị trí cuối
        SELECT LAST_VALUE(cauhoi_id) OVER (ORDER BY cauhoi_id DESC)
            INTO value_last FROM cauhoi;

        -- Thêm dữ liệu
        IF (value_last >= 1) THEN
            INSERT INTO cauhoi (cauhoi_id, chuong_id, cauhoi_name)
                VALUES (value_last+1, p_chuong_id, p_name);
        ELSE
            INSERT INTO cauhoi (cauhoi_id, chuong_id, cauhoi_name)
                VALUES (1, p_chuong_id, p_name);
        END IF;
    END IF;
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_cauhoi(
    p_id           INT,
    p_chuong_id    INT,
    p_cautraloi_id INT,
    p_name         TEXT
) AS
$$
BEGIN
    UPDATE CauHoi
        SET chuong_id = p_chuong_id,
            cautraloi_id = p_cautraloi_id,
            cauhoi_name = p_name
        WHERE cauhoi_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Cập nhập cauhoi với cautraloi_id */
CREATE OR REPLACE PROCEDURE update_cauhoi_cautraloi(
    p_id           INT,
    p_cautraloi_id INT
) AS
$$
BEGIN
    UPDATE cauhoi
        SET cautraloi_id = p_cautraloi_id
        WHERE cauhoi_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Cập nhập cauhoi với chuong_id */
CREATE OR REPLACE PROCEDURE update_cauhoi_chuong(
    p_id        INT,
    p_chuong_id INT
) AS
$$
BEGIN
    UPDATE CauHoi
        SET chuong_id = p_chuong_id
        WHERE cauhoi_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Cập nhập cauhoi với cauhoi_name */
CREATE OR REPLACE PROCEDURE update_cauhoi_name(
    p_id   INT,
    p_name TEXT
) AS
$$
BEGIN
    UPDATE CauHoi
        SET cauhoi_name = p_name
        WHERE cauhoi_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Xóa bảng CauHoi */
CREATE OR REPLACE PROCEDURE delete_cauhoi(
    p_id INT
) AS $$
BEGIN
     -- Bỏ khóa ngoại của CauHoi
    ALTER TABLE cauhoi DROP CONSTRAINT fk_cautraloi_cho_cauhoi;

    -- Xóa các bảng CauTraLoi có cùng cauhoi_id
    DELETE FROM cautraloi WHERE cauhoi_id = p_id;

    -- Xóa bảng CauHoi có cùng cauhoi_id
    DELETE FROM cauhoi WHERE cauhoi_id = p_id;

    -- Add the foreign key constraint back to CauHoi
    ALTER TABLE cauhoi ADD CONSTRAINT fk_cautraloi_cho_cauhoi
        FOREIGN KEY (cautraloi_id) REFERENCES cautraloi(cautraloi_id);
END;
$$ LANGUAGE plpgsql;