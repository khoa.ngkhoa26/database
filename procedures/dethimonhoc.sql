/* Insert */
CREATE OR REPLACE PROCEDURE insert_dethimonhoc(
    p_monhoc_id INT,
    p_name      TEXT,
    p_real      BOOLEAN,
    p_questions INT,
    p_time      INT
) AS
$$
DECLARE
    log_count  INT;
    log_array  INT[] := ARRAY[]::INT[];
    value_last INT;
    item       INT;
BEGIN
    -- Lấy tổng giá trị trong bảng log.dethimonhoc
    SELECT COUNT(log.dethimonhoc.dethimonhoc_id) INTO log_count FROM log.dethimonhoc;

    -- Nếu tổng giá trị trong bảng log.dethimonhoc lớn hơn thì thêm với id từ bảng
    IF (log_count > 0) THEN
        log_array := ARRAY (
            SELECT log.dethimonhoc.public_dethimonhoc_id FROM log.dethimonhoc
                ORDER BY log.dethimonhoc.public_dethimonhoc_id ASC LIMIT 1
        );

        item := log_array[1];

        -- Thêm dữ liệu
        INSERT INTO dethimonhoc (
            dethimonhoc_id,
            monhoc_id,
            dethimonhoc_name,
            dethimonhoc_real,
            dethimonhoc_questions,
            dethimonhoc_time
        ) VALUES (item, p_monhoc_id, p_name, p_real, p_questions, p_time);
    ELSE
        -- Lấy vị trí cuối
        SELECT LAST_VALUE(dethimonhoc_id) OVER (ORDER BY dethimonhoc_id DESC)
            INTO value_last FROM dethimonhoc;

        -- Thêm dữ liệu
        IF (value_last >= 1) THEN
            INSERT INTO dethimonhoc (
                dethimonhoc_id,
                monhoc_id,
                dethimonhoc_name,
                dethimonhoc_real,
                dethimonhoc_questions,
                dethimonhoc_time
            ) VALUES (value_last+1, p_monhoc_id, p_name, p_real, p_questions, p_time);
        ELSE
            INSERT INTO dethimonhoc (
                dethimonhoc_id,
                monhoc_id,
                dethimonhoc_name,
                dethimonhoc_real,
                dethimonhoc_questions,
                dethimonhoc_time
            ) VALUES (1, p_monhoc_id, p_name, p_real, p_questions, p_time);
        END IF;
    END IF;
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_dethimonhoc(
    p_id        INT,
    p_monhoc_id INT,
    p_name      TEXT,
    p_real      BOOLEAN,
    p_questions INT,
    p_time      INT
) AS
$$
BEGIN
    UPDATE dethimonhoc
    SET
        monhoc_id = p_monhoc_id,
        dethimonhoc_name = p_name,
        dethimonhoc_real = p_real,
        dethimonhoc_questions = p_questions,
        dethimonhoc_time = p_time
    WHERE dethimonhoc_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Delete */
CREATE OR REPLACE PROCEDURE delete_dethimonhoc(p_id INT)
AS
$$
BEGIN
    DELETE FROM dethimonhoc
    WHERE dethimonhoc_id = p_id;
END;
$$
LANGUAGE plpgsql;