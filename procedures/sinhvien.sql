/* Insert */
CREATE OR REPLACE PROCEDURE insert_sinhvien(
    p_nguoidung_account TEXT,
    p_name              TEXT,
    p_gender            TEXT,
    p_birthday          TIMESTAMP WITH TIME ZONE
) AS
$$
DECLARE
    log_count  INT;
    log_array  INT[] := ARRAY[]::INT[];
    value_last INT;
    item       INT;
BEGIN
    -- Lấy tổng giá trị trong bảng log.sinhvien
    SELECT COUNT(log.sinhvien.sinhvien_id) INTO log_count FROM log.sinhvien;

    -- Nếu tổng giá trị trong bảng log.sinhvien lớn hơn thì thêm với id từ bảng
    IF (log_count > 0) THEN
        log_array := ARRAY (
            SELECT log.sinhvien.public_sinhvien_id FROM log.sinhvien
                ORDER BY log.sinhvien.public_sinhvien_id ASC LIMIT 1
        );

        item := log_array[1];

        -- Thêm dữ liệu
        INSERT INTO sinhvien (
                sinhvien_id,
                nguoidung_account,
                sinhvien_name,
                sinhvien_gender,
                sinhvien_birthday
            ) VALUES (item, p_nguoidung_account, p_name, p_gender, p_birthday);
    ELSE
        -- Lấy vị trí cuối
        SELECT LAST_VALUE(sinhvien_id) OVER (ORDER BY sinhvien_id DESC)
            INTO value_last FROM sinhvien;

        -- Thêm dữ liệu
        IF (value_last >= 1) THEN
            INSERT INTO sinhvien (
                sinhvien_id,
                nguoidung_account,
                sinhvien_name,
                sinhvien_gender,
                sinhvien_birthday
            ) VALUES (value_last+1, p_nguoidung_account, p_name, p_gender, p_birthday);
        ELSE
            INSERT INTO sinhvien (
                sinhvien_id,
                nguoidung_account,
                sinhvien_name,
                sinhvien_gender,
                sinhvien_birthday
            ) VALUES (1, p_nguoidung_account, p_name, p_gender, p_birthday);
        END IF;
    END IF;
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_sinhvien(
    p_id                INT,
    p_nguoidung_account TEXT,
    p_name              TEXT,
    p_gender            TEXT,
    p_birthday          TIMESTAMP WITH TIME ZONE
) AS
$$
BEGIN
    UPDATE sinhvien
    SET
        nguoidung_account = p_nguoidung_account,
        sinhvien_name = p_name,
        sinhvien_gender = p_gender,
        sinhvien_birthday = p_birthday
    WHERE sinhvien_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Delete */
CREATE OR REPLACE PROCEDURE delete_sinhvien(p_id INT)
AS
$$
BEGIN
    DELETE FROM sinhvien
    WHERE sinhvien_id = p_id;
END;
$$
LANGUAGE plpgsql;