/* Thêm Dethi chính ngẫu nhiên */
CREATE OR REPLACE PROCEDURE insert_dethichinh_random(
    p_dethimonhoc_id INT,
    p_sinhvien_id    INT
) AS
$$
DECLARE
    monhoc_cua_lophoc_sinhvien INT[] := ARRAY[]::INT[];
    monhoc_cua_dethi           INT;
    value_startdate            TIMESTAMP WITH TIME ZONE;
    value_real                 BOOLEAN;
    value_time                 INT;
    value_last                 INT;
    item                       INT;
BEGIN
    -- Lấy môn học của lớp học có chứa sinh viên
    monhoc_cua_lophoc_sinhvien := ARRAY (
        SELECT mh.monhoc_id FROM monhoc mh
            INNER JOIN lophoc lh ON lh.lophoc_id = mh.lophoc_id
            INNER JOIN dangkylophoc dklh ON dklh.lophoc_id = lh.lophoc_id
            INNER JOIN sinhvien sv ON sv.sinhvien_id = dklh.sinhvien_id
            WHERE sv.sinhvien_id = p_sinhvien_id
    );

    -- Lấy môn học của dethimonhoc
    SELECT mh.monhoc_id INTO monhoc_cua_dethi FROM monhoc mh
        INNER JOIN dethimonhoc dtmh ON dtmh.monhoc_id = mh.monhoc_id
        WHERE dtmh.dethimonhoc_id = p_dethimonhoc_id;

    -- Lấy thời gian bắt đầu của thoigiandethichinh trùng với dethimonhoc_od
    SELECT tg.thoigiandethichinh_time INTO value_startdate FROM thoigiandethichinh tg
        INNER JOIN dethimonhoc dtmh ON dtmh.dethimonhoc_id = tg.dethimonhoc_id
        WHERE dtmh.dethimonhoc_id = p_dethimonhoc_id;

    -- Lấy giá trị dethimonhoc_real
    SELECT dtmh.dethimonhoc_real INTO value_real FROM dethimonhoc dtmh
        WHERE dtmh.dethimonhoc_id = p_dethimonhoc_id;

    -- Kiểm tra value_real có phải là true không
    IF (value_real = true) THEN
        -- Kiểm tra nếu monhoc_cua_dethi có trong monhoc_cua_lophoc_sinhvien
        FOREACH item IN ARRAY monhoc_cua_lophoc_sinhvien LOOP
            IF (monhoc_cua_dethi = item) THEN
                -- Lấy dữ liệu cuối
                SELECT LAST_VALUE(dethi_id) OVER (ORDER BY dethi_id DESC) INTO value_last FROM DeThi;

                -- Lấy dữ liệu CauHoi
                SELECT dethimonhoc_time INTO value_time FROM dethimonhoc;

                -- Thêm dữ liệu
                IF (value_last >= 1) THEN
                    INSERT INTO dethi (
                        dethi_id,
                        dethimonhoc_id,
                        sinhvien_id,
                        dethi_startdate,
                        dethi_enddate
                    ) VALUES (
                        value_last+1,
                        p_dethimonhoc_id,
                        p_sinhvien_id,
                        value_startdate,
                        value_startdate + make_interval(mins => value_time)
                    );
                ELSE
                    INSERT INTO dethi (
                        dethi_id,
                        dethimonhoc_id,
                        sinhvien_id,
                        dethi_startdate,
                        dethi_enddate
                    ) VALUES (
                        1,
                        p_dethimonhoc_id,
                        p_sinhvien_id,
                        value_startdate,
                        value_startdate + make_interval(mins => value_time)
                    );
                END IF;
            END IF;
        END LOOP;
    END IF;
END;
$$ LANGUAGE plpgsql;


/* Thêm Dethi thử ngẫu nhiên */
CREATE OR REPLACE PROCEDURE insert_dethithu_random(
    p_dethimonhoc_id INT,
    p_sinhvien_id    INT
) AS
$$
DECLARE
    monhoc_cua_lophoc_sinhvien INT[] = ARRAY[]::INT[];
    monhoc_cua_dethi           INT;
    value_startdate            TIMESTAMP WITH TIME ZONE;
    value_time                 INT;
    value_last                 INT;
    item                       INT;
BEGIN
    -- Lấy môn học của lớp học có chứa sinh viên
    monhoc_cua_lophoc_sinhvien := ARRAY (
        SELECT mh.monhoc_id FROM monhoc mh
            INNER JOIN lophoc lh ON lh.lophoc_id = mh.lophoc_id
            INNER JOIN dangkylophoc dklh ON dklh.lophoc_id = lh.lophoc_id
            INNER JOIN sinhvien sv ON sv.sinhvien_id = dklh.sinhvien_id
            WHERE sv.sinhvien_id = p_sinhvien_id
    );

    -- Lấy môn học của dethimonhoc
    SELECT mh.monhoc_id INTO monhoc_cua_dethi FROM monhoc mh
        INNER JOIN dethimonhoc dtmh ON dtmh.monhoc_id = mh.monhoc_id
        WHERE dtmh.dethimonhoc_id = p_dethimonhoc_id;

    -- Kiểm tra nếu monhoc_cua_dethi có trong monhoc_cua_lophoc_sinhvien
    FOREACH item IN ARRAY monhoc_cua_lophoc_sinhvien LOOP
        IF (monhoc_cua_dethi = item) THEN
            -- Lấy dữ liệu cuối
            SELECT LAST_VALUE(dethi_id) OVER (ORDER BY dethi_id DESC) INTO value_last FROM DeThi;

            -- Lấy dữ liệu CauHoi
            SELECT dethimonhoc_time INTO value_time FROM dethimonhoc;

            SELECT NOW() AT TIME ZONE 'Asia/Ho_Chi_Minh' INTO value_startdate;

            -- Thêm dữ liệu
            IF (value_last >= 1) THEN
                INSERT INTO dethi (
                    dethi_id,
                    dethimonhoc_id,
                    sinhvien_id,
                    dethi_startdate,
                    dethi_enddate
                ) VALUES (
                    value_last+1,
                    p_dethimonhoc_id,
                    p_sinhvien_id,
                    value_startdate,
                    value_startdate + make_interval(mins => value_time)
                );
            ELSE
                INSERT INTO dethi (
                    dethi_id,
                    dethimonhoc_id,
                    sinhvien_id,
                    dethi_startdate,
                    dethi_enddate
                ) VALUES (
                    1,
                    p_dethimonhoc_id,
                    p_sinhvien_id,
                    value_startdate,
                    value_startdate + make_interval(mins => value_time)
                );
            END IF;
        END IF;
    END LOOP;
END;
$$ LANGUAGE plpgsql;