/* Insert */
CREATE OR REPLACE PROCEDURE insert_chuong(
    p_monhoc_id INT,
    p_number    INT,
    p_name      TEXT
) AS
$$
DECLARE
    log_count  INT;
    log_array  INT[] := ARRAY[]::INT[];
    value_last INT;
    item       INT;
BEGIN
    -- Lấy tổng giá trị trong bảng log.chuong
    SELECT COUNT(log.chuong.chuong_id) INTO log_count FROM log.chuong;

    -- Nếu tổng giá trị trong bảng log.chuong lớn hơn thì thêm với id từ bảng
    IF (log_count > 0) THEN
        log_array := ARRAY (
            SELECT log.chuong.public_chuong_id FROM log.chuong
                ORDER BY log.chuong.public_chuong_id ASC LIMIT 1
        );

        item := log_array[1];

        -- Thêm dữ liệu
        INSERT INTO chuong (chuong_id, monhoc_id, chuong_number, chuong_name)
            VALUES (item, p_monhoc_id, p_number, p_name);
    ELSE
        -- Lấy vị trí cuối
        SELECT LAST_VALUE(chuong_id) OVER (ORDER BY chuong_id DESC)
            INTO value_last FROM chuong;

        -- Thêm dữ liệu
        IF (value_last >= 1) THEN
            INSERT INTO chuong (chuong_id, monhoc_id, chuong_number, chuong_name)
                VALUES (value_last+1, p_monhoc_id, p_number, p_name);
        ELSE
            INSERT INTO chuong (chuong_id, monhoc_id, chuong_number, chuong_name)
                VALUES (1, p_monhoc_id, p_number, p_name);
        END IF;
    END IF;
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_chuong(
    p_id        INT,
    p_monhoc_id INT,
    p_number    INT,
    p_name      TEXT
) AS
$$
BEGIN
    UPDATE chuong
    SET
        monhoc_id = p_monhoc_id,
        chuong_number = p_number,
        chuong_name = p_name
    WHERE chuong_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Delete */
CREATE OR REPLACE PROCEDURE delete_chuong(p_id INT)
AS
$$
BEGIN
    DELETE FROM chuong
    WHERE chuong_id = p_id;
END;
$$
LANGUAGE plpgsql;